-- phpMyAdmin SQL Dump
-- version 4.4.3
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 05, 2018 at 02:10 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `ams`
--

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE IF NOT EXISTS `articles` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `subject_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sub_title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci,
  `details` text COLLATE utf8_unicode_ci,
  `image` varchar(150) COLLATE utf8_unicode_ci DEFAULT NULL,
  `display` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`id`, `user_id`, `subject_id`, `title`, `sub_title`, `slug`, `summary`, `details`, `image`, `display`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'Sunt molestias impedit quidem libero in deserunt tempora. Sint tenetur sit et optio tempore ullam itaque.', 'Saepe debitis odit aut necessitatibus illum tenetur.', 'sunt-molestias-impedit-quidem-libero-in-deserunt-tempora-sint-tenetur-sit-et-optio-tempore-ullam-itaque', 'Omnis ullam dolorem praesentium impedit ducimus assumenda illo. Magni velit repudiandae id. Quo consequatur qui est eum voluptas. Quasi quidem in ut et molestiae laboriosam laudantium. Quasi maiores cupiditate ea aspernatur occaecati in consequatur. Aut repellendus voluptatem unde eos.', 'Ipsam magni necessitatibus velit reiciendis fugit cumque. Et ipsum culpa laboriosam qui. Similique dolores vel nobis sint est. Ducimus nisi magnam recusandae omnis exercitationem quo labore. Aut omnis tempore nam ut odit et magnam. Quisquam cum et sint sunt voluptate distinctio fugiat dignissimos. Eius eos voluptatum saepe quis adipisci. Ut qui necessitatibus totam exercitationem. Tempore harum vel hic quasi odit. Nam beatae quae sunt asperiores voluptas. Odio velit maiores a sunt accusantium. Voluptatem sed quia excepturi neque. Totam et qui optio et. Est amet doloribus rerum omnis nihil eos officiis. Rerum non a et numquam et quibusdam provident enim. Eos eaque ipsum consequatur odit accusantium. Aut eos minus quod. Sit modi enim vero culpa. Est eius praesentium mollitia ut consequuntur aut. Similique id provident et iure. Ducimus nostrum voluptas quo. Voluptatem maiores soluta maxime quia quasi quaerat. Architecto aut nesciunt doloremque. Atque necessitatibus tempora ut sunt omnis. Aspernatur atque corrupti enim voluptatem magnam dolorem molestiae ab. Voluptate illo consequatur odio possimus. Et ratione ut quis quas voluptas enim dolor maiores. Exercitationem delectus saepe ut provident quisquam.', '0a4c6e8ffe2669cdd1ec19e8cf9a86b3.jpg', 'Y', '2018-04-30 10:54:30', '2018-08-04 14:40:46', NULL),
(2, 1, 6, 'Id corporis ipsa laborum qui. Minus reiciendis repellendus voluptatem nulla debitis suscipit ipsa est.', 'Minima amet corrupti reprehenderit odit consequatur.', 'id-corporis-ipsa-laborum-qui-minus-reiciendis-repellendus-voluptatem-nulla-debitis-suscipit-ipsa-est', 'Similique nulla ducimus voluptas debitis molestiae aut quaerat. Assumenda dolore voluptatibus sapiente qui quibusdam. Et ut et omnis commodi laboriosam ut magni. Consequatur omnis ut autem debitis consequatur eos. Dolor quidem aut fugiat.', 'Assumenda libero id culpa ex laborum quasi quisquam iste. Velit rem recusandae ut reiciendis. Molestias nemo aliquid totam iste atque. Minus dolorem qui adipisci velit. Rem praesentium voluptatem sit ut ea sit. Reprehenderit nihil dolore est consequuntur omnis rem. Rerum illo recusandae facere quisquam veritatis sint. Qui dicta qui ut odio voluptatem. Deserunt qui soluta hic libero dolorem. Saepe perspiciatis iste illo enim officiis facere. Maxime sit rerum ratione magnam dicta ut. Corrupti et ad et nobis et laborum omnis amet. Temporibus est sapiente quam placeat et aut. Ipsum vitae vel earum ea. Pariatur labore placeat et quisquam amet. Et quasi qui reprehenderit optio illum. Reiciendis commodi eaque explicabo beatae. Incidunt officia accusantium molestiae sint distinctio dolor aliquam. Voluptatem ad illo ut. Qui hic voluptate id consectetur. Quia qui nulla at nostrum natus impedit consequatur. Ut voluptas doloremque ducimus ipsum molestiae voluptas. Fugiat voluptatum quae voluptatem voluptatem placeat rem distinctio.', '8254c96f26dafc42e630f0eb337b7816.jpg', 'Y', '2018-02-13 08:34:28', '2018-08-04 14:40:46', NULL),
(3, 1, 4, 'Omnis voluptate quia temporibus et. Quos rerum rerum ut at in. Debitis ea qui et fuga sapiente ea distinctio sit.', 'Ratione voluptas quo eum enim.', 'omnis-voluptate-quia-temporibus-et-quos-rerum-rerum-ut-at-in-debitis-ea-qui-et-fuga-sapiente-ea-distinctio-sit', 'Facilis placeat ut qui quod. Qui saepe atque iure cupiditate alias nesciunt. Officiis expedita ab repudiandae corrupti aut. Delectus qui est officiis labore temporibus corrupti hic.', 'Quas officiis explicabo fugiat saepe et facere. Consequatur cum nemo non corrupti porro minus voluptatem. Sunt autem non omnis corporis in eum. Necessitatibus dolor et nihil aut earum. Aut qui tempore dolorum iusto. Nostrum animi voluptas repellendus quo similique est. Voluptatem omnis atque laboriosam ut harum. Et blanditiis omnis sed reiciendis. Expedita dolorem voluptas et nihil aliquam. Impedit ullam excepturi laudantium neque est. Commodi quae nisi ipsum sapiente sint soluta quia. Quae quam minus quia eos. Aliquam distinctio explicabo saepe dignissimos. Quae eveniet in quisquam ipsum. Est animi dolorem tenetur ullam voluptatem repellendus. Non quis molestiae assumenda laborum aliquam aliquid repellendus. Accusamus doloremque itaque corporis rerum. Eum modi a ullam consequatur saepe et. Saepe delectus ut accusamus ut tenetur. Esse et non voluptatum accusamus ipsam omnis quos. Odio ea sapiente non aliquam nihil unde tenetur qui. Possimus consequatur quae illo iure et ut. Quas optio et ut id necessitatibus. Voluptatem ipsa consectetur eius voluptates tempore in qui. Maiores mollitia sunt enim in.', 'c7966ba7dbadf944b5ef6c60fe06f4cb.jpg', 'Y', '2017-09-02 01:19:56', '2018-08-04 14:40:46', NULL),
(4, 1, 4, 'Eos quia ducimus et modi quis sit ut. In voluptates repudiandae quo officia. Nobis nostrum veniam molestiae atque.', 'Optio sint quibusdam animi.', 'eos-quia-ducimus-et-modi-quis-sit-ut-in-voluptates-repudiandae-quo-officia-nobis-nostrum-veniam-molestiae-atque', 'Corrupti accusantium eius sunt eius voluptatem iure quis. Quia perferendis assumenda omnis ut sint. Et cum et qui laboriosam dolorem. Ut quas exercitationem saepe quae. Itaque voluptas laborum expedita nesciunt eveniet. Quis est itaque facilis consequuntur laborum temporibus.', 'Velit voluptates possimus odit a aut. Quis eius quaerat a voluptatem enim omnis earum minima. Dolor repellendus ut repellat explicabo vitae corporis. Tempora et commodi quasi perferendis qui. Consequatur sint numquam ab qui excepturi rerum ducimus aut. Accusantium fugiat aperiam quibusdam voluptate. Sit consequatur ut sit sed enim iure. Autem non sed architecto facere officiis magni repudiandae perspiciatis. Facere incidunt magnam eum dolore dolores. Impedit est vel et et rerum. Voluptatem dolor aut dolor animi harum. Beatae debitis ipsum sit ea aperiam. Excepturi cumque optio a commodi amet ea. Error eligendi enim sit facilis ipsum non accusamus unde. Ducimus itaque delectus impedit rerum dolorem et. Ut recusandae eum praesentium accusantium nulla. Unde illo aut quam quod odio consequatur eaque. Illo sunt sunt quibusdam quo nam in. Est et velit earum maxime ad. In ducimus quis iusto odit voluptatem tenetur non. Voluptatem odit sunt et unde. Officiis deleniti facilis autem voluptatem quasi. Dicta repellendus distinctio enim dicta ut totam et. Est optio officiis suscipit dignissimos minima. Nesciunt enim eos quae et sed eaque. Placeat et rerum veritatis qui quia. Sequi non molestiae sunt aperiam et velit iusto. Quos magni corrupti quo harum aliquam voluptates.', 'e32370fc867b072f3308bfa0bac1970b.jpg', 'Y', '2017-09-05 19:34:32', '2018-08-04 14:40:46', NULL),
(5, 1, 2, 'Aspernatur alias nisi architecto voluptatum autem quo. Sed tenetur quidem vero assumenda eos.', 'Libero blanditiis autem qui est illum in.', 'aspernatur-alias-nisi-architecto-voluptatum-autem-quo-sed-tenetur-quidem-vero-assumenda-eos', 'Sed recusandae ut labore eum voluptas et. Sint maxime consequatur ullam voluptatem. Dignissimos ab dolore molestiae ut ad. Aut sunt et sunt assumenda in explicabo qui. Illum rerum libero dolores excepturi.', 'Alias est reiciendis in id pariatur ut voluptates. Ullam nam quae qui molestiae ut corporis. Architecto qui eos sed laboriosam repellendus hic sed. Suscipit doloremque est eos ut. Velit harum ipsam temporibus eos. Blanditiis ut laborum ut doloremque dignissimos laboriosam molestiae qui. Nesciunt veniam ipsam delectus consectetur a in sunt quia. Non perferendis exercitationem adipisci optio. Accusamus doloribus deleniti voluptatem in cumque veniam ducimus et. Ex similique enim commodi et ipsa maxime quo voluptatem. Dolores porro explicabo molestiae et dolorem dolorum distinctio. Unde corrupti unde doloremque laborum sit. Ipsam qui culpa et quam beatae.', '873bcc46f261c94cba1ea29857ad08f6.jpg', 'Y', '2018-04-06 03:32:41', '2018-08-04 14:40:46', NULL),
(6, 2, 5, 'Quidem enim nisi voluptatum dolore eveniet. Iure at labore voluptas officia.', 'Accusamus tenetur velit qui voluptas.', 'quidem-enim-nisi-voluptatum-dolore-eveniet-iure-at-labore-voluptas-officia', 'Nihil et quae occaecati deleniti enim. Optio exercitationem officia iste vel hic nisi aspernatur tenetur. Consequatur non et optio est ea totam est quam. Aut sint ea officiis dolor consequuntur hic sunt. Quos ea qui neque numquam ut incidunt odio.', 'Officia assumenda laboriosam necessitatibus qui odio quo voluptatem. Commodi tempore incidunt mollitia dolore. Quia saepe dolor et dolorem nulla voluptatum reprehenderit. Iusto autem doloremque voluptas. Architecto doloremque molestiae iusto. Veniam dolores odio rerum fugiat. Magni odit velit aperiam voluptas esse. Dolor dolorem laudantium occaecati eum consequatur. Magni sunt a aut molestias similique dignissimos in. Doloribus eligendi quas voluptatem sunt sit. Beatae quidem delectus quia velit repudiandae doloremque vero. Voluptatem voluptatem est aliquid laudantium enim eum quia. Quasi non vel natus ullam. Vero quis amet a et.', '143255dade9787c815e33e4ee7e328c1.jpg', 'Y', '2018-01-20 07:23:56', '2018-08-04 14:41:04', NULL),
(7, 2, 1, 'Eaque ex vel voluptas quis voluptates. Veniam quidem ipsa ullam aut ipsum rerum alias.', 'Amet vel ut et velit doloremque sit.', 'eaque-ex-vel-voluptas-quis-voluptates-veniam-quidem-ipsa-ullam-aut-ipsum-rerum-alias', 'Est non odit fuga fugit voluptas incidunt consectetur. In modi est sed enim expedita aliquam dolor nemo. Quibusdam voluptatem quos nemo ut. Quisquam et at veritatis consequatur cum culpa temporibus. Sed explicabo sunt atque eligendi ab laborum et.', 'Quis eum voluptatem optio. Voluptatem optio autem aliquam ab corporis suscipit est omnis. Necessitatibus laboriosam perferendis sapiente. Ad fuga qui inventore consequatur dolorem. Ab rerum molestiae consequuntur non ut. Voluptate tempore quia quae natus praesentium sint. Est et velit recusandae optio aperiam excepturi accusantium. Maiores aut impedit maiores. Cupiditate officiis dolores aut autem rerum qui. Minima et laudantium voluptatibus sed ratione consequuntur. Dignissimos magni architecto expedita est ea deleniti. Omnis aut odio ut dolores nulla. Provident doloribus sit sunt qui dolore vel. Eos vel sit aut optio. Corrupti hic quia nam aut quo. Dolore tempore nam enim natus quod quia ad. Minus deleniti aspernatur itaque sint doloribus. Doloribus a consequatur qui earum delectus illum aut. Aliquam sunt nobis quidem earum quo.', '915583f7d351ffa70d0c26e1946d3859.jpg', 'Y', '2018-03-23 13:44:26', '2018-08-04 14:41:04', NULL),
(8, 2, 3, 'Corrupti accusamus consequatur qui. Commodi consequuntur sunt impedit quae quia.', 'Blanditiis nam laudantium et.', 'corrupti-accusamus-consequatur-qui-commodi-consequuntur-sunt-impedit-quae-quia', 'Aliquam magni odit ut. Totam quibusdam qui aut odio. Nesciunt quia est velit corporis consequatur.', 'Maxime velit qui ut itaque voluptatem ut aut. Et et necessitatibus nobis. Eos tempora qui sapiente dolorem iusto assumenda et. Consequuntur cum exercitationem est dolor cupiditate dolor. Et nemo soluta ipsum nemo. Occaecati vitae quas rerum numquam. Omnis ipsa quas nam qui expedita nulla laudantium animi. Vel cupiditate harum est dignissimos veritatis impedit consequuntur. Repudiandae maiores repudiandae molestiae dolorem odit eum. Eos in dolor enim incidunt magni. Vero ut molestiae et corporis dolorem vel ipsa. Sunt optio nihil dignissimos inventore quam corrupti ut. Eligendi vel autem qui sint provident. Libero voluptas perspiciatis et ut quibusdam deserunt vitae. Quia maiores dignissimos eveniet. Odit temporibus molestias consequatur tempore nesciunt minima. Ex nostrum animi quis. Et omnis suscipit provident quisquam et laudantium. Saepe id vitae voluptas. Consequatur aut eos exercitationem nesciunt deserunt voluptatem asperiores. Facere eligendi dolores praesentium iste. Commodi consequatur quo reprehenderit. Placeat molestias ab neque culpa earum libero cum. Ullam incidunt laboriosam eveniet rem voluptates et asperiores vitae.', '36bd1378181a1c9fc37465e3269772b2.jpg', 'Y', '2017-10-19 05:53:25', '2018-08-04 14:41:04', NULL),
(9, 2, 4, 'Voluptatem ex aut ut architecto. Quos dolorem veritatis maiores. Autem enim aut quia voluptatem maxime et.', 'Voluptatem nihil rem libero impedit.', 'voluptatem-ex-aut-ut-architecto-quos-dolorem-veritatis-maiores-autem-enim-aut-quia-voluptatem-maxime-et', 'Iusto quisquam minima dicta earum libero voluptas vitae. Ipsum est earum nesciunt nostrum adipisci sed assumenda. Dolores repudiandae voluptatem asperiores impedit quis qui.', 'Maxime placeat quis molestias qui. Fugit ut non tempora alias quo quo. Cum soluta enim fugit cumque. Optio distinctio ullam commodi quia commodi. Ducimus dicta qui provident quam incidunt sunt. Est vel laudantium eum beatae. Qui et eligendi voluptatem cupiditate provident voluptate. Distinctio voluptatum ut delectus voluptates sapiente culpa quisquam. Autem deserunt incidunt temporibus facere fuga adipisci. Aut sed at consequatur consequatur. Ratione nobis velit et officiis beatae. Et dolores vel et architecto. Rem harum est saepe animi velit necessitatibus fugiat. Ea inventore earum nihil recusandae veniam earum velit numquam. Consequuntur ullam aut eum blanditiis. Corrupti illum reprehenderit debitis qui voluptatem ipsum beatae. Quia quam sed soluta voluptatem. Fugiat dolor sed fugiat iste aspernatur. Deserunt omnis inventore exercitationem in accusamus dolor porro voluptas. Beatae vel maiores perspiciatis velit animi vel. Sed sunt id nam asperiores. Repellendus dolor vero voluptatum culpa. Voluptatem voluptatem nihil ullam incidunt repellat. Sint nobis expedita doloremque qui. Aut sed rerum in unde qui in.', 'd94db6cf7bdec80ab221448ffb67830c.jpg', 'Y', '2018-07-17 13:00:12', '2018-08-04 14:41:04', NULL),
(10, 2, 6, 'Iure cupiditate suscipit nihil libero. Eos eos possimus a. Esse quo quibusdam provident doloremque voluptates.', 'Quia reprehenderit fugit nihil est dolorem.', 'iure-cupiditate-suscipit-nihil-libero-eos-eos-possimus-a-esse-quo-quibusdam-provident-doloremque-voluptates', 'Tempore non sit qui a modi. Autem at molestias similique. Ea labore autem non dolorem esse fugiat quam quia. Eligendi cumque accusamus quis explicabo. Voluptatem dolorum est aut.', 'Excepturi aspernatur qui sit similique eum. Animi hic reiciendis eveniet deleniti. Delectus laudantium ab totam dolores. Enim non veritatis natus eligendi ut quidem laborum. Qui labore in distinctio officiis sit doloribus dolorem. Nihil eum sit qui. Nihil vero autem et vel beatae repellat. Sapiente ut voluptatibus doloremque at. Optio nisi impedit sit dolor necessitatibus facilis et. Quod repellendus magni et et expedita deleniti sequi. Non nihil omnis fuga delectus est nisi delectus. Est et id dolores eos perspiciatis laboriosam. Est facere eius nisi sit. Qui quia veniam quam earum.', '4267ddc803f7a222655e38bba2e0a541.jpg', 'Y', '2018-07-01 12:22:35', '2018-08-04 14:41:04', NULL),
(11, 3, 2, 'Et veniam at aut. Consequatur et dolor illum sed illo quam similique.', 'Animi maiores et incidunt magnam consequatur sapiente.', 'et-veniam-at-aut-consequatur-et-dolor-illum-sed-illo-quam-similique', 'Ex at mollitia odit qui. Qui recusandae voluptatem et non. Consequatur rerum ipsum ullam animi.', 'Rerum praesentium aperiam sint iure iusto quia fugiat. Illo in et sequi dicta velit voluptas tenetur. Sit atque quis laboriosam animi. Velit unde vel dolorum ipsam repellat dicta vero. Ducimus inventore rerum ipsam quae et hic. Illum in ipsa dignissimos. Non cupiditate mollitia ab quam rem impedit. Voluptatem ullam modi consequatur qui in iste officiis. Qui voluptatem esse sit qui officia. Esse quibusdam incidunt corrupti est deserunt ut tempora. Itaque vero omnis officia aspernatur. Est qui dignissimos quam repudiandae est. Id et libero corrupti nobis cumque non suscipit. Illum totam qui quos nisi dicta. Maxime ab et qui a minus rerum veritatis. Explicabo explicabo reiciendis est a quis voluptas facere et. Mollitia est ipsa libero. Voluptas voluptatem laudantium cum. Magnam ipsum dolorem minima sint. Illum consequatur veniam cumque saepe et. Id fuga iste autem sunt aliquam incidunt ratione. Nobis ut tempore doloremque ratione voluptatum. Quia eum deserunt et incidunt rerum. Qui nam vel maiores necessitatibus delectus ullam earum. Fugiat dolores alias eum eos et. Provident id molestias eveniet tempore in. Cumque accusamus aspernatur facere ut ea totam. Vel voluptatem quo autem quo aut placeat ipsum animi.', '88bb647e26500f021614b30116ec6f6d.jpg', 'Y', '2017-12-14 16:54:28', '2018-08-04 14:41:21', NULL),
(12, 3, 5, 'Molestias sed aut aut. Vitae quo quaerat aspernatur fugit labore. Molestiae sunt molestiae est minus rerum quo modi.', 'Culpa modi unde exercitationem eligendi.', 'molestias-sed-aut-aut-vitae-quo-quaerat-aspernatur-fugit-labore-molestiae-sunt-molestiae-est-minus-rerum-quo-modi', 'Ullam sequi laboriosam laborum qui ipsam et. Enim aut natus quia repudiandae. Numquam sit voluptatibus autem ipsa perferendis veniam voluptatem. Ad quis sit et eum aut at.', 'Minima quia aliquid et necessitatibus debitis impedit provident placeat. Error vel mollitia dolorem rerum sed. Qui sint cum beatae. Maiores excepturi id consequuntur et ut accusantium. Temporibus sit modi voluptas quas distinctio vel. Optio eum dolorem quis omnis. Tempore nisi quibusdam voluptatem eos est. Facilis libero maiores quae esse molestias ad. Officia maiores voluptatem aut occaecati. Vero harum expedita error natus soluta fugit. Et quos vel nesciunt incidunt corporis rerum. Odio quo voluptatem quisquam eos quod rerum. Earum deleniti qui quas et sed et dolorem. Enim laboriosam ullam aut omnis veniam fuga deleniti. Culpa et omnis occaecati enim. Rerum ad ex quisquam reprehenderit sit. Accusantium asperiores distinctio quam.', '3e63b693d7866838c7cfe1a288eb81d5.jpg', 'Y', '2018-02-28 14:39:23', '2018-08-04 14:41:21', NULL),
(13, 3, 1, 'Vel similique eum et omnis quis. Autem culpa maxime veritatis rem. Ullam voluptate deleniti nostrum hic fuga.', 'Reiciendis esse ratione aut.', 'vel-similique-eum-et-omnis-quis-autem-culpa-maxime-veritatis-rem-ullam-voluptate-deleniti-nostrum-hic-fuga', 'Rerum impedit reiciendis ut quo doloremque. Aperiam velit autem corrupti molestiae et nesciunt. Sint id dolorem et et ducimus. Hic optio necessitatibus aut animi porro.', 'Officiis quo consequatur ipsum placeat aliquam dicta natus saepe. Voluptatibus ut dolores neque sed beatae rem. Molestias rem fugit et optio itaque. Non delectus dolores fugiat. Et qui minus beatae voluptates. Recusandae nihil aliquam cumque incidunt numquam libero sequi. In nisi vero accusamus. Harum ut et eaque. Consequatur omnis veritatis vero quidem et sapiente. Veritatis dolores a ratione repellat sed facere. Corrupti omnis incidunt vitae tempora enim. Aut fugiat eveniet officiis similique maxime dolorum rerum. Et dolore voluptas omnis voluptate perspiciatis distinctio veritatis. Ipsa inventore architecto alias repellat. Omnis autem explicabo aut eligendi laborum enim. Voluptatem est debitis repellat enim. Facere et harum sit eos quas et. Aut tenetur voluptas et occaecati. Veritatis aut ea quia saepe quia dolores ipsa veniam. Officia dolorem aut quasi non. Sequi aliquam eius et mollitia nihil. Rerum dolore corporis quis consequuntur officiis voluptas. Ullam et et sed incidunt qui. Velit harum consequatur incidunt omnis doloribus voluptatibus dignissimos. Iusto eum exercitationem unde rem eos cum provident.', 'e43b6ebed26de6c933554af5bce60a88.jpg', 'Y', '2018-05-01 18:17:54', '2018-08-04 14:41:21', NULL),
(14, 3, 2, 'Quibusdam similique et sit quia est fuga. Qui ipsa repellendus beatae aliquam aut consectetur non. Non dicta eius et.', 'Itaque et at ea molestiae.', 'quibusdam-similique-et-sit-quia-est-fuga-qui-ipsa-repellendus-beatae-aliquam-aut-consectetur-non-non-dicta-eius-et', 'Inventore velit qui consequatur id aperiam sed. Ducimus exercitationem dolor nostrum minima voluptatem. Minima et quisquam qui corrupti vel dolor neque velit.', 'Velit dolor sed autem dolores earum. Esse error quam voluptatem amet. Consequatur ut in aut aut aperiam ut minima. Quia expedita id sint. Nisi non et rerum. Molestias et eos aut laboriosam est ad. Laboriosam consequatur perspiciatis aspernatur et natus aperiam. Voluptatem incidunt est provident sit qui quas. Vel nihil similique adipisci sapiente optio rem laborum molestiae. Labore inventore perspiciatis beatae tempore et. Ullam eum quam et porro quod minus perferendis. Iure culpa quasi earum velit totam officia cum. Vitae voluptas qui consequatur sit eum. Sapiente quos enim doloribus dolor. Accusamus sint distinctio inventore commodi iure soluta. Est et est ex illo voluptate amet. Sit quo amet cumque minima aspernatur laborum aspernatur. Voluptas nesciunt enim et sapiente voluptatem tempore excepturi. Qui quae incidunt et est. Quam est ipsum aut sequi consequatur. Perspiciatis voluptatem quia incidunt sit inventore officiis. Tenetur commodi iste et iste maxime corrupti explicabo. Est impedit qui et numquam qui est dolore. Est voluptas in ut est est id vitae. Earum aut unde molestias provident voluptatibus ut. Natus et incidunt sunt. Impedit saepe id et qui repellat exercitationem rem dolores. Natus est sunt animi voluptatem.', '0ffe385bcf2d09297345880aa6bcc5ed.jpg', 'Y', '2018-02-03 02:04:00', '2018-08-04 14:41:21', NULL),
(15, 3, 3, 'Et ut molestiae magni unde ut. Nihil et est minima totam aliquid soluta. Voluptates non enim ut in reprehenderit ut.', 'Sed sunt et suscipit aut autem sint.', 'et-ut-molestiae-magni-unde-ut-nihil-et-est-minima-totam-aliquid-soluta-voluptates-non-enim-ut-in-reprehenderit-ut', 'Aut explicabo et dolore odio eum id. Ut a molestias officiis est tempore maiores exercitationem. Quasi omnis non et nam accusantium.', 'Eveniet sapiente quia impedit totam amet. Aliquid ea iure facilis odio sint ut voluptate. Non dicta quis assumenda voluptas vel expedita. Eos mollitia vel laudantium atque. Quas nisi et amet veritatis. Aut omnis ut iure distinctio. Non eligendi cumque consectetur fugit asperiores voluptatem. Aut deleniti et repellat et nobis ut. Ullam eligendi ea quam et velit. Qui distinctio magni magnam reprehenderit. Ad quod aliquam optio dolorem repellendus aut aut. Aspernatur esse ipsam reprehenderit architecto cupiditate animi quisquam. Assumenda praesentium sit commodi quis dolores rem quis. Atque deserunt voluptatum saepe ab. Amet rerum sapiente veniam. Dolorem deleniti et et. Ipsam ullam explicabo ipsum cupiditate quia. Impedit tenetur commodi inventore saepe eos molestiae tempora. Quam reiciendis similique deserunt labore debitis rem quia adipisci.', '22b376527053eec8e7d9d3c466410eab.jpg', 'Y', '2017-09-02 13:18:35', '2018-08-04 14:41:21', NULL),
(16, 4, 7, 'Id cupiditate facere voluptatum non. Blanditiis in natus nihil quo. Reiciendis fuga enim qui qui.', 'Repudiandae adipisci nulla tempora eius sit eaque.', 'id-cupiditate-facere-voluptatum-non-blanditiis-in-natus-nihil-quo-reiciendis-fuga-enim-qui-qui', 'Velit iure repellat sapiente nulla. Et nobis culpa dignissimos dolorem reiciendis dolorem. Et quasi ut doloremque dolorem dolores vel expedita. Aut voluptatibus sit non sit. Error suscipit sapiente accusamus voluptas quod molestiae qui.', 'Et ipsam distinctio quos iste maiores. Et ut perferendis aut possimus officia. Et nesciunt omnis doloremque quo nihil. Recusandae eligendi et magnam repellendus qui. Quasi nam eum consequatur fugiat id reprehenderit at. Optio asperiores rerum et dolores. Rerum possimus nostrum consequatur libero. Reprehenderit sint architecto quibusdam est. Alias sapiente veritatis debitis assumenda perspiciatis sit. Expedita exercitationem nesciunt quo dolor delectus aliquid. Numquam sint officiis tempora quaerat optio. Delectus rem tenetur sint illum. Nihil libero at voluptas sint vero. Aspernatur placeat eum molestiae dolor. Voluptate distinctio inventore necessitatibus porro aliquid fuga qui. Qui natus tempora fugiat quia facere. Expedita mollitia autem pariatur quis qui repudiandae. Doloremque ex nobis nihil. Sequi non laboriosam quis ex cupiditate sint. Occaecati dolore dicta neque maiores placeat.', 'fca2b31572ec0aff802e2e63c3c9f8a9.jpg', 'Y', '2017-10-08 16:08:30', '2018-08-04 14:41:34', NULL),
(17, 4, 2, 'Aut corrupti recusandae aut vitae dolorem. A quam sed rem officia. Et voluptates rerum et non similique ut.', 'Iure nesciunt earum harum.', 'aut-corrupti-recusandae-aut-vitae-dolorem-a-quam-sed-rem-officia-et-voluptates-rerum-et-non-similique-ut', 'Et qui in dicta voluptatem molestias assumenda quod. Consequatur nobis dolorem accusamus perferendis voluptatem dolorum. Cupiditate architecto deleniti odio deserunt dolor quia ipsum. Recusandae repellat neque et corrupti quia id rerum.', 'Sed aut tempore quia aut at laboriosam. Soluta laborum in et optio tempore similique. Voluptatem autem dolorem illo facilis. Natus deleniti vitae sunt totam aut quo. In vel culpa accusantium voluptatem similique sit. Explicabo ut vel velit. Voluptatem praesentium rerum facilis deleniti. Enim animi ab unde nihil magni qui. Et et rerum delectus illum et beatae nobis. Perferendis facere quia enim provident. Enim ut et velit vel. Sit error dignissimos quis rerum neque quia. Consequatur consequatur fugit excepturi aliquid at quos et doloremque. Quos rem nihil nostrum distinctio cum dicta. Perferendis omnis nihil vel a amet ad. Voluptas sapiente id itaque. Saepe et enim a omnis. Voluptatem totam qui non est nobis. Repudiandae alias quo nobis. Eligendi reprehenderit consectetur libero quia. Aut magnam tenetur autem necessitatibus saepe natus iste.', '722056943ac385182858b6890715bcf0.jpg', 'Y', '2017-09-30 12:50:08', '2018-08-04 14:41:35', NULL),
(18, 4, 7, 'Voluptas omnis hic est. Voluptates ut rerum dolores sequi eveniet est facilis. Voluptatum fuga enim aut vero.', 'Ad velit velit illo enim libero.', 'voluptas-omnis-hic-est-voluptates-ut-rerum-dolores-sequi-eveniet-est-facilis-voluptatum-fuga-enim-aut-vero', 'Esse laudantium facere asperiores sit est veniam magnam sint. Aut est nostrum aut beatae aspernatur ut consequuntur quis. Amet qui rerum doloribus inventore est.', 'Quo magni ut adipisci eum. Minima fuga quos consectetur mollitia voluptatem possimus pariatur dolorem. Temporibus enim consequatur rerum molestias ratione ea. Ut illo velit veniam. Et unde laboriosam et fugiat expedita quisquam quis. Delectus a nam aspernatur amet. Illum ea laudantium expedita ut et. Ut illum qui molestiae reprehenderit ipsa qui facilis. Magnam saepe et occaecati praesentium nemo. Fuga ad perspiciatis temporibus id explicabo placeat. Aut qui sapiente cum doloribus quia neque. Consequatur minima odio illum sed et consectetur maiores. Voluptas in commodi voluptatum et tempore vel. Dolorum ullam assumenda laboriosam cumque eveniet vel sed. Fuga quibusdam et maiores officia. Quia in aut dolorem culpa. Dicta id consequuntur velit enim sit. Possimus aperiam voluptates facere cumque nemo vel. Quae itaque et eligendi nostrum blanditiis magnam sapiente. Asperiores necessitatibus inventore dolor ut quae accusamus iure. Saepe excepturi temporibus in est quasi sed et. Aliquam repellat quasi nihil. Quod deleniti sed dolorem non reprehenderit voluptas iure. A eveniet sint sapiente corrupti.', '030c7b7782419d2c2479515ebc6e4b51.jpg', 'Y', '2017-09-16 11:33:22', '2018-08-04 14:41:35', NULL),
(19, 4, 2, 'Maiores laboriosam sequi sit est quidem laborum. Magnam quam reiciendis eos.', 'Eos aut officiis consequatur voluptas voluptates.', 'maiores-laboriosam-sequi-sit-est-quidem-laborum-magnam-quam-reiciendis-eos', 'Nihil esse asperiores occaecati molestiae dolores nesciunt. Voluptas quos aut itaque quia natus totam. Laboriosam provident et eius saepe fugiat ex quia. Ut error alias aliquid quia autem hic.', 'Dolore qui modi neque vel ducimus illo aut. Nihil nihil qui quod perferendis amet repellat. Voluptas repellendus sunt velit culpa sint distinctio facilis. Non fuga rerum impedit non assumenda possimus. Ea deleniti est ipsam illum. Quisquam aut eveniet nihil ipsa sit omnis. Quo distinctio eaque qui suscipit aut magnam. Facilis explicabo perferendis possimus ad. Sed cumque cum consequatur quis dolorum est mollitia. Eum incidunt consequatur nemo deserunt ut. Inventore officiis magni veniam est. Tempore deserunt aliquid sed laboriosam soluta. Expedita quod molestiae quidem quis.', 'ec9361398edf3ee8671f12003948c232.jpg', 'Y', '2017-12-05 00:30:27', '2018-08-04 14:41:35', NULL),
(20, 4, 6, 'Ut veniam dicta iure. In ratione asperiores voluptatem excepturi omnis veniam. Molestiae reiciendis quis at aut.', 'Ipsam accusantium sed incidunt reprehenderit unde.', 'ut-veniam-dicta-iure-in-ratione-asperiores-voluptatem-excepturi-omnis-veniam-molestiae-reiciendis-quis-at-aut', 'Id et magni eum dolor praesentium dolorem facere similique. Suscipit omnis vitae eum ab est ex et. Nihil possimus voluptas facilis ea doloribus explicabo commodi. Vel harum explicabo blanditiis velit voluptas. Error consequatur voluptatum dolorem quasi. Sed consequatur numquam veritatis sint veritatis ea.', 'Voluptatem voluptatibus incidunt consequuntur facere quasi. Tempore exercitationem minima neque. Quia saepe dignissimos in voluptates quaerat sunt quod. Perferendis nostrum possimus doloribus excepturi debitis assumenda voluptas. Sint ea commodi doloribus illum. Molestias consequatur quaerat nobis itaque. Magnam esse nemo a et et ipsum dolor. Ut eos quidem voluptatem perspiciatis. Quia reprehenderit voluptatem recusandae a molestias modi voluptatem rem. Dolor eligendi perspiciatis ut. Est dolorem mollitia voluptas non ipsum. Consequuntur voluptatem repellendus assumenda magnam quos qui. Et sint id autem nesciunt quasi illo odio molestiae. Aut at beatae maxime ut quia quia. Saepe laborum distinctio et. Delectus maiores in tempore totam quasi autem.', '9a4cac31cf178ad41c531621cd9801a2.jpg', 'Y', '2017-12-19 15:29:52', '2018-08-04 14:41:35', NULL),
(21, 5, 2, 'Temporibus doloribus sed sit non. Et et minus explicabo aliquid. Voluptate cum accusantium illum.', 'Ducimus fuga nihil voluptatem.', 'temporibus-doloribus-sed-sit-non-et-et-minus-explicabo-aliquid-voluptate-cum-accusantium-illum', 'Officiis id id dolores enim. Ut non quam eum amet consequatur et quis. Tenetur expedita quae necessitatibus repellendus non molestiae. Voluptatem illum consequatur esse voluptatum. Sit reprehenderit in rem eum omnis non accusantium.', 'Maxime ut unde dolor quia perspiciatis saepe. Porro iure debitis rerum autem. Incidunt reprehenderit aut voluptatibus ipsa. Non fugiat consequatur non eveniet pariatur facilis aliquid. Et incidunt nihil molestiae ut deserunt voluptates error qui. Quia beatae asperiores nostrum placeat necessitatibus aut molestias. Ducimus nemo voluptates et quas voluptatem. Ut hic ab eligendi ducimus est similique. Dolor fugit et sunt. Aliquid consequatur dolor porro et officia. Veritatis sed ut doloribus voluptate mollitia ratione consequatur. Et magni vitae molestias distinctio. Culpa in dolor aut rerum exercitationem vel. Iure mollitia nisi ipsa ullam et occaecati ut. Quam voluptatum mollitia dolor cumque ullam pariatur magni. Nihil alias pariatur ab deleniti quasi culpa exercitationem natus. Aspernatur ipsum omnis alias beatae amet autem. Dicta et vitae qui quaerat sed tempore at est. Magni ex aut ut vel consequuntur asperiores. Blanditiis similique quia ea et. Est officia est deleniti voluptas modi odio. Autem et quis eum officiis.', 'c25edd02b6fe6cc2251b07947a99f398.jpg', 'Y', '2017-09-21 08:54:51', '2018-08-04 14:41:55', NULL),
(22, 5, 4, 'Eum et necessitatibus mollitia illo sapiente assumenda provident. A aut quia aut sequi autem iure explicabo.', 'Amet consequuntur dolores nesciunt porro est ipsa.', 'eum-et-necessitatibus-mollitia-illo-sapiente-assumenda-provident-a-aut-quia-aut-sequi-autem-iure-explicabo', 'Ut similique molestiae rem nemo quo. Et impedit voluptatem sint accusamus aperiam. Illum consequatur consectetur ab quod quam velit nihil. Ut ut et totam voluptatibus aliquam. Laboriosam sunt quasi rerum repudiandae iure ut.', 'Et sed ut ea tenetur. Hic saepe eveniet in laboriosam voluptatibus. Sit repellat non et sit provident sit. Dignissimos distinctio deserunt consequatur hic. Dolores quis porro officia aut temporibus. Reprehenderit omnis voluptatem reprehenderit delectus. Dolor dolor sequi consectetur quis voluptatem quod vel neque. Et omnis error saepe quis sed officia et. Dolore tempora adipisci facere aperiam atque. Autem aut reiciendis qui saepe ducimus quasi autem sed. Eius quos ducimus tempora necessitatibus molestias rerum neque eum. Vel dolorum non molestiae repellendus cum maxime. Beatae nostrum quo quis nulla beatae omnis. Omnis dignissimos veniam ut ipsa. Nisi ea et eos reiciendis nihil voluptatem. Harum expedita et est. Ut repellat quasi consequatur rem fugit quos sed. Velit aut id ea maxime molestiae. Officiis est voluptas ex laudantium autem iure commodi. Quas qui porro molestiae eos et necessitatibus. Quia excepturi vero culpa. Sed sint velit omnis occaecati quaerat.', '14815d18efd0533d6a9125f977236f1d.jpg', 'Y', '2018-01-21 13:09:20', '2018-08-04 14:41:55', NULL),
(23, 5, 3, 'Iusto eaque blanditiis impedit officia. Et dolor voluptatem ut officia modi. Sapiente iusto est id vel et.', 'Recusandae quas consequuntur minima.', 'iusto-eaque-blanditiis-impedit-officia-et-dolor-voluptatem-ut-officia-modi-sapiente-iusto-est-id-vel-et', 'Voluptas dolores error sit voluptates sint quia molestias. Autem unde est vitae natus optio ipsa. Magni eos pariatur similique iste itaque sed similique. Id rerum aut aperiam aliquam ut. Fugiat occaecati facere minima at quas perferendis atque.', 'Rerum sapiente accusamus dolore est numquam velit ipsum voluptatem. Fuga excepturi voluptatem et odio. Officiis reprehenderit et odio nostrum. Molestiae nostrum enim perspiciatis officiis. Nulla occaecati tempore non. Ipsam sint minima rerum ea. Atque minima tenetur error aliquam impedit odit. Et quas consequuntur voluptate commodi eos officiis. Molestiae non non laudantium aut deleniti et. Sunt soluta dolores inventore perferendis facilis libero. Voluptates nostrum vel laborum ullam tenetur et quis. Sequi sunt molestiae fugit facere ipsa. Ea ducimus nemo quia veritatis magnam aut. Nam magni autem fugit ut enim nostrum ut amet. Minima officia magni numquam omnis beatae. Vero magnam perferendis non et voluptatem sapiente in quaerat. Sed qui et ut aut accusantium qui est. Alias voluptatem possimus sunt omnis facere. Eveniet sed culpa qui sed deleniti voluptatem consequatur. Culpa sapiente et dolorem aut inventore ipsa excepturi. Similique neque earum excepturi ut et iusto. Id voluptatem sit quo doloribus et quos. Excepturi sunt perferendis optio quis consequuntur praesentium. Officiis earum sequi facilis saepe. Adipisci iusto ut consequatur qui sed. Possimus architecto eos dignissimos eos nisi doloribus. Sit sed fugiat hic ducimus iure. Atque officia tenetur doloremque.', '2e3dce16e4468b9fb35532cb6c6a2b70.jpg', 'Y', '2017-10-26 21:22:26', '2018-08-04 14:41:55', NULL),
(24, 5, 6, 'A excepturi architecto quia corrupti ipsa. Ex doloremque iusto nostrum nulla quidem non laudantium.', 'Pariatur architecto et quis reiciendis.', 'a-excepturi-architecto-quia-corrupti-ipsa-ex-doloremque-iusto-nostrum-nulla-quidem-non-laudantium', 'Ut voluptates enim enim iusto et. Perferendis expedita eos perspiciatis nemo quo. Fugiat est nemo quibusdam voluptatem. Voluptatem corporis ullam ut magni vero sunt totam. Officiis velit tempora saepe debitis.', 'Voluptatem enim libero quia soluta perspiciatis velit est. Vitae nulla quia ducimus voluptates voluptatum itaque fuga assumenda. Exercitationem et consequatur veniam. Eos voluptate velit quibusdam nulla. Expedita sit quia magni consequatur. Consequatur dolores ex voluptatem voluptatem ipsam. Non qui modi rerum nam voluptatem animi est. A voluptatem sit porro neque. Occaecati dolore quidem deserunt. Omnis ad qui aliquid pariatur cupiditate eaque quia. Sapiente possimus similique consequatur ut repudiandae autem beatae. Quo sint veniam accusantium cum. Neque accusamus rerum velit enim aut. Ab assumenda sit quo quos. Molestiae nemo totam quo velit dolorem. Neque magnam aspernatur in quos. Veritatis unde id delectus ut quia occaecati distinctio qui. Facilis ipsa doloremque est voluptatem dicta. Accusantium eveniet numquam suscipit aut quidem esse.', 'f8a55a0a9489275ecc216a791f8c9a44.jpg', 'Y', '2017-12-29 08:17:09', '2018-08-04 14:41:55', NULL),
(25, 5, 1, 'Excepturi laboriosam officia provident eligendi autem eligendi alias. Expedita id fugiat sint nulla.', 'Praesentium error aut quis a ea eum.', 'excepturi-laboriosam-officia-provident-eligendi-autem-eligendi-alias-expedita-id-fugiat-sint-nulla', 'Sed veritatis facilis distinctio et eligendi. Similique doloremque unde non vitae tenetur sapiente perferendis. Nisi quisquam hic voluptatem et rem itaque.', 'Architecto ipsa dignissimos omnis est quidem. Ducimus aut earum quos itaque. Facere autem nesciunt aut. Ab quas aliquid quis omnis. Quibusdam quia voluptatem nulla explicabo non libero. Aut ut alias placeat recusandae. Ut omnis earum autem minima enim. Aut dolore provident ipsam aut nobis. Laborum molestiae consectetur cum sed maiores. Autem consequuntur velit cum. Similique maiores est quidem eum libero iusto. Dolor rem error unde reiciendis labore sapiente et. Voluptatem dolorum quidem sed modi occaecati et. Laboriosam beatae veniam quaerat quas nemo facilis.', 'd2788cd7dd95b1feaecb8acaefdfe884.jpg', 'Y', '2018-01-09 02:39:26', '2018-08-04 14:41:55', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `details` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `display` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `commentable_id` int(10) unsigned NOT NULL,
  `commentable_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `user_id`, `details`, `display`, `commentable_id`, `commentable_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'Repellat laboriosam repudiandae soluta sequi. Est vel ut repellat est aut. Similique omnis rerum eius fugit.', 'Y', 1, 'articles', '2018-01-20 10:36:43', '2018-08-04 14:41:55', NULL),
(2, 1, 'Vero sunt tempora non quia. Laborum sunt corporis ea unde earum. Necessitatibus ut dignissimos sunt non non. Recusandae eius et consequatur sed earum sed. Ut laboriosam error dolor similique in. Qui sint incidunt at.', 'Y', 1, 'articles', '2018-01-15 00:19:45', '2018-08-04 14:41:56', NULL),
(3, 4, 'Ullam est dolor hic nihil sequi voluptatum minima consectetur. Quo odio qui aspernatur quaerat. Et est inventore reiciendis ut harum maxime. Perspiciatis omnis neque iste ex.', 'Y', 1, 'articles', '2018-02-14 20:18:23', '2018-08-04 14:41:56', NULL),
(4, 2, 'Laborum dolore autem ut non voluptatem. Officia eaque qui alias necessitatibus est voluptatem. Quia assumenda quia atque accusantium.', 'Y', 1, 'articles', '2018-03-08 12:34:03', '2018-08-04 14:41:56', NULL),
(5, 5, 'Suscipit maiores beatae ut sed ullam et. Dolorem saepe aut et culpa dignissimos vitae. Placeat unde sed corporis aut quam placeat est ipsa.', 'Y', 1, 'articles', '2017-10-07 08:45:48', '2018-08-04 14:41:56', NULL),
(6, 1, 'Nulla eos suscipit sit quis porro inventore quas. Quidem porro facilis quibusdam cumque consequuntur eos repellendus. Ex inventore aut architecto autem. Est ullam est itaque laboriosam.', 'Y', 2, 'articles', '2017-08-10 21:11:00', '2018-08-04 14:41:56', NULL),
(7, 4, 'Quaerat debitis eligendi dolor. Veritatis quos soluta fugit praesentium. Vel autem quia voluptatem sunt. Provident velit voluptatum omnis possimus odit.', 'Y', 2, 'articles', '2017-10-26 23:54:12', '2018-08-04 14:41:56', NULL),
(8, 2, 'Laudantium accusamus modi qui aut. Rerum facere dolorem dolorum odio. Animi occaecati accusamus mollitia facilis nisi. Eligendi aspernatur occaecati facere beatae et autem quis. Modi non officiis neque libero modi.', 'Y', 2, 'articles', '2017-08-07 18:41:39', '2018-08-04 14:41:56', NULL),
(9, 1, 'Atque doloremque odio aut ea eum. Quibusdam odit ut nesciunt occaecati totam quibusdam eos. Voluptas odio iusto unde minima sed. Incidunt tenetur voluptatem sed eos. Eum a nisi magni. Dolorem nihil impedit maiores.', 'Y', 2, 'articles', '2018-07-07 20:10:40', '2018-08-04 14:41:56', NULL),
(10, 4, 'Dolorum omnis vel nobis rerum pariatur ut ea. Magnam odio voluptatem sed rerum. Et optio fugit porro consequatur.', 'Y', 2, 'articles', '2018-07-20 10:13:10', '2018-08-04 14:41:56', NULL),
(11, 2, 'Dolor animi ut corrupti amet eaque ut. Velit vel accusantium ut et neque quia delectus. Quisquam non quidem hic rem illo adipisci aspernatur. Provident enim incidunt voluptas possimus doloribus laborum id velit.', 'Y', 2, 'articles', '2017-12-08 18:38:38', '2018-08-04 14:41:56', NULL),
(12, 3, 'Praesentium sed eligendi vitae minus soluta laborum velit. Rem consectetur eaque laboriosam cumque voluptatem rerum est. Deleniti unde nihil aut architecto. Minima quo at eos non sed.', 'Y', 3, 'articles', '2018-04-30 16:01:39', '2018-08-04 14:41:56', NULL),
(13, 1, 'Repellat quia non voluptas molestiae suscipit. Quo nam neque tempora ipsam nesciunt et. Quia odit et culpa cupiditate iste.', 'Y', 3, 'articles', '2018-02-04 11:47:52', '2018-08-04 14:41:56', NULL),
(14, 3, 'Aut quas quia iste mollitia vel earum. Quia facere pariatur quia omnis est veritatis libero.', 'Y', 4, 'articles', '2018-04-30 18:46:36', '2018-08-04 14:41:56', NULL),
(15, 1, 'Qui impedit enim repudiandae laboriosam. Voluptatibus aut unde vero rerum eaque natus. Incidunt magni ad vel laudantium quis.', 'Y', 4, 'articles', '2017-12-06 08:15:50', '2018-08-04 14:41:56', NULL),
(16, 5, 'Et consequatur molestiae mollitia. Incidunt sed voluptatum exercitationem beatae quo. Odio suscipit laborum eaque ea ipsa sapiente et. Qui tenetur necessitatibus et consectetur sapiente ratione fuga.', 'Y', 4, 'articles', '2017-09-04 22:28:15', '2018-08-04 14:41:56', NULL),
(17, 1, 'Eveniet consequatur tenetur vel cum. Aut aut aliquid incidunt iusto rerum.', 'Y', 4, 'articles', '2017-11-15 14:25:14', '2018-08-04 14:41:56', NULL),
(18, 3, 'Facere id aperiam eum inventore vel odio accusamus. Laudantium tenetur optio sit nostrum officiis vitae. Id perferendis fugit alias a assumenda iusto a et. Maxime ut perspiciatis placeat sit in.', 'Y', 4, 'articles', '2017-11-05 13:32:55', '2018-08-04 14:41:56', NULL),
(19, 5, 'Necessitatibus deleniti non non dolor explicabo exercitationem. Quibusdam voluptatem sint quia laborum voluptas voluptatem. Vel adipisci accusamus quo et alias animi.', 'Y', 4, 'articles', '2017-10-31 17:31:16', '2018-08-04 14:41:56', NULL),
(20, 1, 'Qui autem dolorem est reiciendis fugiat doloremque voluptas. Tenetur eaque in corrupti itaque perspiciatis ab.', 'Y', 4, 'articles', '2018-02-28 11:19:39', '2018-08-04 14:41:56', NULL),
(21, 1, 'Eveniet molestiae aut nesciunt eum magni quaerat. Consequatur soluta ducimus in blanditiis. Et inventore et soluta aperiam voluptas sit quos.', 'Y', 4, 'articles', '2018-01-11 10:26:12', '2018-08-04 14:41:56', NULL),
(22, 5, 'Non ut laborum asperiores consectetur quis. Quo autem culpa saepe ut. Libero totam atque laborum voluptatem ea.', 'Y', 5, 'articles', '2017-08-24 15:50:44', '2018-08-04 14:41:56', NULL),
(23, 1, 'Sed voluptates et voluptatem minima. Illo omnis nihil qui et et harum cumque. Possimus molestiae vero sit vel enim aliquam earum.', 'Y', 5, 'articles', '2017-12-06 22:31:07', '2018-08-04 14:41:56', NULL),
(24, 3, 'Quisquam a ut perferendis in. Ipsum consequuntur non aperiam. Cum veniam nobis voluptatem porro est illo.', 'Y', 5, 'articles', '2018-05-02 07:37:39', '2018-08-04 14:41:56', NULL),
(25, 5, 'Provident quasi ea suscipit facere aliquam fugiat. Non iusto illo molestias eveniet quas ad ut nesciunt. Qui natus quo et debitis. Tenetur est iure repellat temporibus qui et ducimus.', 'Y', 5, 'articles', '2018-03-05 06:55:16', '2018-08-04 14:41:56', NULL),
(26, 4, 'Laborum eos repellat vel voluptates laboriosam quibusdam consectetur quas. Ea cupiditate occaecati eum quia quia id ut reiciendis. Aut aut atque qui voluptatem. Corporis hic saepe et a maiores et qui.', 'Y', 5, 'articles', '2017-10-23 18:28:04', '2018-08-04 14:41:56', NULL),
(27, 3, 'Animi delectus distinctio recusandae molestiae voluptates. Officia corporis eveniet cum provident. Provident eligendi ut et qui aut iste laboriosam. Sit eum iusto quo.', 'Y', 5, 'articles', '2018-07-21 15:40:42', '2018-08-04 14:41:56', NULL),
(28, 1, 'Earum illum corporis porro explicabo reprehenderit id ullam. Aut est debitis numquam sit.', 'Y', 5, 'articles', '2018-02-21 00:59:01', '2018-08-04 14:41:56', NULL),
(29, 5, 'Veniam id perferendis quisquam. Nam vel aut tempore. Ipsum optio qui fugit. Magnam ipsa suscipit sed voluptates.', 'Y', 5, 'articles', '2018-02-24 20:15:45', '2018-08-04 14:41:56', NULL),
(30, 2, 'Iure eligendi modi ad quod asperiores facere assumenda. Ullam porro sit et sunt voluptatem voluptatem eos fuga.', 'Y', 6, 'articles', '2017-08-13 11:30:39', '2018-08-04 14:41:56', NULL),
(31, 5, 'Consequatur non amet distinctio vel. Nesciunt dignissimos consequatur cumque quis autem ut est. Veritatis at tempora est quos aliquam esse voluptatibus.', 'Y', 6, 'articles', '2018-06-21 21:38:01', '2018-08-04 14:41:56', NULL),
(32, 1, 'Nulla molestiae officia quo quidem magni non et accusamus. Culpa aliquam cum similique perferendis. Animi nihil dicta laudantium eum rerum voluptate. Impedit ut voluptates suscipit ut corrupti qui.', 'Y', 6, 'articles', '2018-05-21 19:17:23', '2018-08-04 14:41:56', NULL),
(33, 1, 'Quos odit rerum maxime blanditiis non reiciendis et. Distinctio dolor quisquam ratione voluptates sed consequatur voluptas odio. Alias voluptatem molestiae molestiae quos ut dolor eligendi eos.', 'Y', 6, 'articles', '2017-11-27 19:55:46', '2018-08-04 14:41:57', NULL),
(34, 4, 'Illum in nam aut est et consectetur. Sint earum ut cum corporis dolores id blanditiis ipsum. Quia provident possimus et aut.', 'Y', 7, 'articles', '2017-10-30 05:02:54', '2018-08-04 14:41:57', NULL),
(35, 2, 'Voluptatem nihil nesciunt dolores illo iusto temporibus. Eaque sequi quis itaque beatae laborum. Delectus quos vel veritatis consequuntur.', 'Y', 7, 'articles', '2018-06-18 00:49:26', '2018-08-04 14:41:57', NULL),
(36, 2, 'Recusandae et aliquam architecto aut et voluptatum ut. Perspiciatis nemo eos adipisci delectus. Hic molestias qui rerum est provident. Officiis repellat qui aliquid.', 'Y', 7, 'articles', '2018-07-19 20:25:46', '2018-08-04 14:41:57', NULL),
(37, 3, 'Voluptas sequi vero aperiam aut. Qui nihil eligendi et labore sed tenetur enim nisi. Neque aliquid aspernatur quis et iusto possimus veniam a. Voluptatibus adipisci beatae et qui nulla excepturi vero.', 'Y', 7, 'articles', '2018-05-11 12:34:13', '2018-08-04 14:41:57', NULL),
(38, 5, 'Aperiam in impedit totam qui aut aliquid in. Eaque et beatae corrupti. Aperiam sit officia eius atque consectetur.', 'Y', 7, 'articles', '2018-05-26 08:28:11', '2018-08-04 14:41:57', NULL),
(39, 4, 'Porro similique nihil ut sunt dolor. Quia deserunt qui architecto eum quam. Voluptate laborum non iste nulla exercitationem qui ullam. Ea sed deleniti laudantium accusamus enim.', 'Y', 7, 'articles', '2017-08-23 17:33:43', '2018-08-04 14:41:57', NULL),
(40, 5, 'Ipsam et numquam velit aut. Amet beatae voluptas quo est quis atque. Suscipit ratione et qui iure. Odio illum blanditiis et molestiae molestias omnis.', 'Y', 7, 'articles', '2017-08-31 18:33:48', '2018-08-04 14:41:57', NULL),
(41, 4, 'Ipsum sit ea sit maiores. Accusantium commodi iure ducimus id. Ratione perferendis dolores voluptatum. Qui delectus aut aut est qui.', 'Y', 7, 'articles', '2017-08-29 04:28:46', '2018-08-04 14:41:57', NULL),
(42, 3, 'Et molestias sit officia sed culpa occaecati nostrum est. Nulla et eaque ut ut sunt harum quae. Et nam sed dignissimos et. Voluptatem quo quaerat accusamus deleniti.', 'Y', 8, 'articles', '2018-04-24 18:01:08', '2018-08-04 14:41:57', NULL),
(43, 4, 'Consequatur assumenda fugit at et ut expedita. Minus suscipit qui magnam illo amet rerum dolores porro. Quia temporibus qui id mollitia ratione aut dolorem. Sequi ducimus dolores et ea.', 'Y', 8, 'articles', '2018-04-17 02:35:18', '2018-08-04 14:41:57', NULL),
(44, 5, 'Aut reiciendis quis et nobis. Qui quia voluptatem nam quidem. Soluta porro nesciunt quidem qui minima fuga ut. Suscipit veniam voluptas similique fuga maiores incidunt sit.', 'Y', 8, 'articles', '2018-02-01 22:43:15', '2018-08-04 14:41:57', NULL),
(45, 3, 'Cupiditate excepturi voluptatibus deleniti doloribus neque perferendis est. Id veniam dolorem quisquam quo. Ut tenetur architecto similique eos tenetur.', 'Y', 8, 'articles', '2017-11-30 03:01:16', '2018-08-04 14:41:57', NULL),
(46, 5, 'Explicabo maxime velit occaecati facere deleniti. Eum ut dolore praesentium quia. Iste amet enim nisi iure. Earum aperiam quod et voluptatem voluptatum.', 'Y', 8, 'articles', '2017-12-03 23:22:42', '2018-08-04 14:41:57', NULL),
(47, 1, 'Et dignissimos ducimus consectetur ut eum aut. Eum asperiores temporibus consectetur. Dolores sapiente beatae vitae vitae facere. Perferendis deleniti harum unde veniam ut vel.', 'Y', 8, 'articles', '2018-02-21 08:23:42', '2018-08-04 14:41:57', NULL),
(48, 3, 'Unde corporis pariatur enim mollitia neque et est. Odit rerum impedit laborum ut cupiditate ullam asperiores autem. Aut facere error earum sint ullam ex.', 'Y', 8, 'articles', '2017-08-20 07:36:10', '2018-08-04 14:41:57', NULL),
(49, 2, 'Quod corporis harum sit consequatur. Dolorum quia magni voluptas deleniti. Esse aspernatur placeat sed ut et eos. Incidunt provident quia natus placeat.', 'Y', 9, 'articles', '2018-05-21 17:22:58', '2018-08-04 14:41:57', NULL),
(50, 5, 'Culpa ea et illum quibusdam. Aut in consequatur non enim iure fuga rerum quos. Aut veniam nihil vel praesentium assumenda eveniet ratione.', 'Y', 9, 'articles', '2018-05-29 17:14:36', '2018-08-04 14:41:57', NULL),
(51, 3, 'Consequatur sapiente vitae pariatur. Ipsum non animi nostrum occaecati necessitatibus fugiat consequatur. Itaque non occaecati non excepturi. Quo quasi magni quae sed provident.', 'Y', 9, 'articles', '2017-08-10 10:17:31', '2018-08-04 14:41:57', NULL),
(52, 4, 'Est quia occaecati adipisci fugit dolorem nobis facilis. Dolorem cupiditate vel autem ea voluptatum necessitatibus eligendi. Incidunt minima ducimus exercitationem soluta quis architecto. Magni quas ut asperiores.', 'Y', 9, 'articles', '2017-10-04 19:51:13', '2018-08-04 14:41:57', NULL),
(53, 2, 'Eligendi illum et voluptatem quia. Omnis quia fuga dolorem. Consequuntur cumque dolorem accusamus pariatur quibusdam.', 'Y', 9, 'articles', '2018-02-28 02:43:55', '2018-08-04 14:41:57', NULL),
(54, 3, 'Ratione dolor exercitationem dolores et nam. Earum accusantium minus fugit rem ullam voluptates. Natus quod molestiae dolorem culpa.', 'Y', 9, 'articles', '2018-03-20 01:59:42', '2018-08-04 14:41:57', NULL),
(55, 2, 'Porro nulla veniam dolore omnis non beatae natus nam. Sit error et architecto pariatur quos debitis qui. Illo facere beatae numquam non. Omnis ut laudantium doloribus quod quos et explicabo.', 'Y', 10, 'articles', '2018-05-31 01:42:17', '2018-08-04 14:41:57', NULL),
(56, 4, 'Ut occaecati consequatur nostrum adipisci consequatur. Itaque voluptatum reprehenderit dolores minima quos. Et aut explicabo nesciunt.', 'Y', 10, 'articles', '2018-04-22 20:21:25', '2018-08-04 14:41:57', NULL),
(57, 2, 'Nemo qui eius ipsa libero. Eum vel incidunt omnis iusto et provident. Vel maiores occaecati ut aliquid assumenda at voluptatibus. Vel ipsum modi consectetur est ea provident et.', 'Y', 10, 'articles', '2017-08-17 22:19:21', '2018-08-04 14:41:57', NULL),
(58, 2, 'Eos ut qui est et hic magni. Eaque praesentium corporis veniam at consequatur voluptas labore.', 'Y', 10, 'articles', '2018-07-07 01:06:21', '2018-08-04 14:41:57', NULL),
(59, 3, 'Hic ut tempore voluptates harum. Vitae nihil nam incidunt est a. Nisi voluptas ratione id repellendus aut fugit. Pariatur vel amet hic laboriosam a dignissimos.', 'Y', 10, 'articles', '2018-01-20 11:32:00', '2018-08-04 14:41:57', NULL),
(60, 3, 'Et delectus quibusdam est. Est totam iure aut ut pariatur est dolores numquam.', 'Y', 10, 'articles', '2018-02-25 02:45:53', '2018-08-04 14:41:57', NULL),
(61, 5, 'Facilis cupiditate blanditiis cumque sit et nemo quod assumenda. Expedita quasi repudiandae esse cum unde. Qui ducimus et omnis qui similique hic.', 'Y', 10, 'articles', '2017-10-10 13:06:06', '2018-08-04 14:41:57', NULL),
(62, 1, 'In iusto consequatur qui impedit est. Debitis saepe qui dolor consectetur officiis velit nostrum asperiores. Ratione cumque asperiores laborum molestiae.', 'Y', 10, 'articles', '2017-11-23 15:00:42', '2018-08-04 14:41:57', NULL),
(63, 1, 'Aspernatur porro sequi necessitatibus molestiae molestiae. Quidem nam itaque illo nemo. Enim ullam et deserunt. Cupiditate enim rerum nulla repudiandae enim assumenda quia.', 'Y', 11, 'articles', '2017-12-19 19:18:06', '2018-08-04 14:41:57', NULL),
(64, 5, 'Qui ut corrupti magnam sit qui. Molestias aut rerum qui iste nemo qui et. Deleniti fugit deserunt eum molestiae quam. Nihil ex reprehenderit eum reprehenderit in doloremque.', 'Y', 11, 'articles', '2018-02-24 00:56:06', '2018-08-04 14:41:57', NULL),
(65, 1, 'Ab nam sit dolorem officiis quas molestiae. Ut temporibus quia inventore tenetur occaecati soluta. Et recusandae et laboriosam est temporibus nam.', 'Y', 11, 'articles', '2017-12-03 23:16:30', '2018-08-04 14:41:57', NULL),
(66, 5, 'Laudantium quia quis tempore quibusdam alias ut et. Aspernatur inventore rerum voluptatem. Sint ut dicta quis ab. Voluptas delectus amet ut consequatur deserunt ut.', 'Y', 11, 'articles', '2018-03-18 05:28:49', '2018-08-04 14:41:57', NULL),
(67, 1, 'Quis nesciunt rerum nihil dicta inventore aut laudantium occaecati. Id natus rem porro iure ut voluptas rerum. Natus ad repellat rerum iusto suscipit qui incidunt. Eveniet alias et fugit est beatae cupiditate ut.', 'Y', 11, 'articles', '2018-01-08 00:20:39', '2018-08-04 14:41:57', NULL),
(68, 3, 'Dolor eius quasi sit voluptas quos. Cumque aliquid eos cumque alias incidunt velit. Et perferendis molestias tempora deleniti itaque commodi. Sed quia illo inventore ipsa quod nam. Aut nam voluptas iusto.', 'Y', 12, 'articles', '2018-04-08 00:47:17', '2018-08-04 14:41:57', NULL),
(69, 1, 'Corporis esse quas sed minus. Aspernatur vel consequatur et. Dolorum aliquid a molestiae enim amet. Temporibus ipsam est doloribus dolorem quo et cum officiis.', 'Y', 12, 'articles', '2018-06-17 12:38:14', '2018-08-04 14:41:58', NULL),
(70, 5, 'Praesentium rerum est temporibus cum quae sed et. Quo aut et id occaecati corporis ut in. Sequi omnis amet architecto cumque sunt numquam ea. Et blanditiis est facilis magni et ut qui.', 'Y', 12, 'articles', '2018-02-15 09:09:26', '2018-08-04 14:41:58', NULL),
(71, 5, 'Nam quidem quo ut sed laborum est rem nam. Aperiam dolorem ullam nihil accusamus illum aut odio. Voluptatem quia beatae corporis esse fuga.', 'Y', 12, 'articles', '2018-05-20 00:53:03', '2018-08-04 14:41:58', NULL),
(72, 4, 'Vel ipsa et sed consectetur accusamus. Aut possimus laboriosam totam rerum ducimus. Aliquam tempora amet repellendus voluptas voluptatem quis.', 'Y', 12, 'articles', '2018-06-17 12:27:44', '2018-08-04 14:41:58', NULL),
(73, 2, 'Adipisci sit molestiae facere qui. Ullam doloribus magnam delectus vero facere et repellat autem. Placeat aut nulla esse voluptatem tempora et deleniti. Maxime dolor odit magni atque maxime eos dolorem.', 'Y', 13, 'articles', '2017-12-10 09:03:54', '2018-08-04 14:41:58', NULL),
(74, 1, 'Omnis explicabo autem quaerat quo fugit necessitatibus. Id ex dolores tenetur excepturi cum. Eos dolorem culpa veritatis cum quod corrupti at. Doloribus voluptatem aspernatur iure magni.', 'Y', 13, 'articles', '2018-05-31 11:46:43', '2018-08-04 14:41:58', NULL),
(75, 2, 'Ducimus sed et voluptate aspernatur sit. Dolor quis ut et consequuntur. Soluta vel assumenda eos est eaque.', 'Y', 13, 'articles', '2018-07-10 06:55:20', '2018-08-04 14:41:58', NULL),
(76, 4, 'Natus doloribus illo maiores numquam facere iusto numquam. Maxime dolores pariatur similique. Temporibus distinctio sed nulla possimus est sit est.', 'Y', 13, 'articles', '2017-08-22 02:49:33', '2018-08-04 14:41:58', NULL),
(77, 1, 'Magni porro porro debitis dolores ipsam. Suscipit dignissimos assumenda quo consequatur et. Aut sed ipsa et quaerat.', 'Y', 13, 'articles', '2017-10-28 07:18:11', '2018-08-04 14:41:58', NULL),
(78, 5, 'Nihil cumque voluptatum non. Ipsum aliquam quo veritatis expedita consequatur non in ad.', 'Y', 13, 'articles', '2018-01-28 02:30:45', '2018-08-04 14:41:58', NULL),
(79, 4, 'Omnis quam deleniti illo vero laborum ut qui. Molestias quae rerum ut labore. Non dolor dolores occaecati ad. Quasi beatae minima non deserunt.', 'Y', 13, 'articles', '2017-10-19 18:19:24', '2018-08-04 14:41:58', NULL),
(80, 2, 'Illum aut non eligendi necessitatibus aut. Ut dicta et a blanditiis voluptatem. Repellendus qui quasi iste. Dolor aspernatur nobis sed est ea illum voluptatem.', 'Y', 13, 'articles', '2018-07-26 08:54:03', '2018-08-04 14:41:58', NULL),
(81, 1, 'Eum quia animi distinctio earum. Sunt provident officia sint. Aspernatur vel facilis totam ut optio debitis.', 'Y', 14, 'articles', '2017-10-19 01:08:22', '2018-08-04 14:41:58', NULL),
(82, 2, 'At quis dolorum laboriosam et ut delectus. Et quis voluptatem reprehenderit sunt aliquam. Modi totam quas vitae nostrum. Delectus maxime quod ullam.', 'Y', 14, 'articles', '2018-02-09 18:20:46', '2018-08-04 14:41:58', NULL),
(83, 4, 'Vitae dolores consequatur omnis voluptatem aliquid vel et. Delectus aut non quisquam ut voluptatibus.', 'Y', 14, 'articles', '2018-03-02 11:55:56', '2018-08-04 14:41:58', NULL),
(84, 3, 'Qui sed ex provident voluptatum quo rerum quasi. Fuga aliquam aut sequi aut sint qui voluptatibus. Voluptatum labore numquam expedita natus et.', 'Y', 14, 'articles', '2018-02-05 08:59:27', '2018-08-04 14:41:58', NULL),
(85, 5, 'Minima eum ut quia alias. Sit sit ullam optio officia alias. Laborum earum nulla quam consequatur quis. Non alias aliquid error aut suscipit. Voluptas labore molestias qui voluptatem veritatis.', 'Y', 14, 'articles', '2018-04-24 00:33:10', '2018-08-04 14:41:58', NULL),
(86, 4, 'Qui magnam fuga voluptatibus fuga enim sit harum. Dignissimos qui culpa ipsa et adipisci. Qui quos voluptatem et error doloribus. Aut velit est nesciunt.', 'Y', 14, 'articles', '2017-12-23 14:38:47', '2018-08-04 14:41:58', NULL),
(87, 4, 'Voluptas explicabo quisquam et sapiente expedita quia vero ratione. Dolorem sed voluptatum sint ullam expedita voluptatum expedita molestias. Id in sit et tempora corporis tempore alias. Hic omnis commodi sit est.', 'Y', 15, 'articles', '2017-09-24 10:35:45', '2018-08-04 14:41:58', NULL),
(88, 1, 'Quasi qui a sint doloremque dolor vitae perferendis. Omnis modi fugiat quia in aliquam. Assumenda nobis et quisquam perspiciatis nostrum. Ipsa beatae eligendi quia mollitia iste sint.', 'Y', 15, 'articles', '2018-02-12 13:52:13', '2018-08-04 14:41:58', NULL),
(89, 1, 'Voluptas id pariatur ipsam incidunt perspiciatis repudiandae quo. Officiis in et velit velit aspernatur in. Quia rem id magni sunt autem aut.', 'Y', 15, 'articles', '2017-10-07 23:04:11', '2018-08-04 14:41:58', NULL),
(90, 3, 'Qui odio occaecati et sit et facilis explicabo. Rem nesciunt est voluptatibus quas ut quibusdam quisquam. Est omnis repudiandae possimus quas ratione vel. Ut earum ipsa pariatur voluptatum et corporis.', 'Y', 15, 'articles', '2018-02-17 07:17:13', '2018-08-04 14:41:58', NULL),
(91, 5, 'Suscipit repellat ullam impedit quis mollitia explicabo et. Tempora at voluptatem qui consequatur est impedit. Sed molestias et nemo iste alias.', 'Y', 15, 'articles', '2018-07-19 09:58:23', '2018-08-04 14:41:58', NULL),
(92, 2, 'Excepturi sunt numquam autem qui voluptatem vel doloribus quia. Neque dolorem rem sit sed. Sed est aut est eaque.', 'Y', 15, 'articles', '2018-01-26 03:12:32', '2018-08-04 14:41:58', NULL),
(93, 2, 'Quisquam repellat modi odio sed maiores sed doloremque. Error magnam laudantium in. At autem soluta aut et.', 'Y', 16, 'articles', '2018-07-05 05:51:56', '2018-08-04 14:41:58', NULL),
(94, 3, 'Rerum asperiores commodi est voluptas qui. Dignissimos laudantium atque qui qui. Atque molestiae ducimus laboriosam assumenda illum accusamus.', 'Y', 16, 'articles', '2017-08-30 14:12:42', '2018-08-04 14:41:58', NULL),
(95, 1, 'Doloribus non esse ipsam sunt architecto. Tempora exercitationem reiciendis suscipit blanditiis. Sunt aut ipsa expedita molestias sequi dolor distinctio a.', 'Y', 16, 'articles', '2018-03-09 07:25:11', '2018-08-04 14:41:58', NULL),
(96, 1, 'Reiciendis iusto ducimus placeat error. Dolor quibusdam consequatur pariatur. Quisquam sequi quisquam dicta nesciunt aperiam nihil. Omnis velit vitae dolorum ratione.', 'Y', 16, 'articles', '2018-02-14 22:30:48', '2018-08-04 14:41:58', NULL),
(97, 3, 'Soluta nihil voluptatem reiciendis. Cumque voluptatem quisquam vel voluptate. Nihil praesentium ipsum doloribus sint provident soluta. Aut debitis omnis et fugit adipisci saepe odio.', 'Y', 17, 'articles', '2018-04-19 01:52:55', '2018-08-04 14:41:58', NULL),
(98, 1, 'Minima in expedita recusandae repudiandae necessitatibus qui. Vitae adipisci omnis rerum soluta. Itaque est molestiae aut sunt quaerat quod vel facere.', 'Y', 17, 'articles', '2017-09-27 23:29:45', '2018-08-04 14:41:58', NULL),
(99, 3, 'Deserunt accusantium voluptatem corrupti eos error totam nobis. Molestias placeat odit voluptatem ut laudantium sed. Nihil veniam exercitationem fugit.', 'Y', 17, 'articles', '2018-05-16 08:38:26', '2018-08-04 14:41:58', NULL),
(100, 1, 'Mollitia rerum fugit est id quia soluta ut. Qui voluptas dolor ab aliquam aut. Expedita ducimus perferendis eum cupiditate laudantium quia. Reprehenderit ipsam minus cum rerum ut aliquam earum.', 'Y', 17, 'articles', '2017-10-02 11:09:04', '2018-08-04 14:41:58', NULL),
(101, 2, 'Nemo est aut labore perspiciatis ut distinctio ducimus. Voluptatibus fuga fuga magnam est architecto qui velit. Et porro necessitatibus qui.', 'Y', 17, 'articles', '2018-03-11 17:07:13', '2018-08-04 14:41:58', NULL),
(102, 1, 'Vitae asperiores distinctio fuga debitis quas debitis sunt. Ad perspiciatis hic maiores est perspiciatis voluptatibus suscipit. Animi sed et dicta.', 'Y', 17, 'articles', '2017-10-08 02:21:13', '2018-08-04 14:41:58', NULL),
(103, 1, 'Dolorem ipsam qui aliquid rerum exercitationem. Sit minima explicabo blanditiis rerum. Aliquid qui maxime ab odio quod. Nihil consequatur eaque nobis amet et.', 'Y', 18, 'articles', '2018-03-08 14:36:21', '2018-08-04 14:41:58', NULL),
(104, 5, 'Consequuntur et voluptas at. Atque ducimus incidunt similique. Atque aut voluptatem placeat quae officia vel.', 'Y', 18, 'articles', '2017-10-28 05:08:29', '2018-08-04 14:41:58', NULL),
(105, 3, 'Libero deserunt beatae officia cumque dolorem fugiat. Non voluptatem culpa placeat velit et. Cupiditate incidunt in aperiam at. Sunt sed sequi velit.', 'Y', 18, 'articles', '2018-02-14 20:16:53', '2018-08-04 14:41:58', NULL),
(106, 2, 'Totam et provident earum quis perspiciatis earum. Vero enim adipisci distinctio ullam. Tempore et atque voluptas et.', 'Y', 18, 'articles', '2018-02-23 04:15:27', '2018-08-04 14:41:58', NULL),
(107, 1, 'Iste id in ipsum laborum praesentium fugit. Et eum perspiciatis sed animi qui.', 'Y', 18, 'articles', '2018-02-11 08:31:42', '2018-08-04 14:41:58', NULL),
(108, 4, 'Id vitae perspiciatis sunt quia illum voluptatem. Nulla necessitatibus iusto culpa voluptatem et inventore. Beatae ex ut qui soluta.', 'Y', 19, 'articles', '2017-11-29 00:54:12', '2018-08-04 14:41:59', NULL),
(109, 3, 'Quam qui doloremque quasi adipisci voluptas ab. Veniam accusamus ullam ut ab dolores. Reiciendis dolor eligendi animi saepe neque ut reprehenderit. Quae repellat maxime ratione ut quaerat.', 'Y', 19, 'articles', '2018-05-10 16:27:23', '2018-08-04 14:41:59', NULL),
(110, 3, 'Distinctio inventore rem qui voluptatem. Qui optio nostrum sequi debitis commodi harum labore. Labore inventore enim quis maxime enim esse. Corporis voluptatem ipsa autem eum autem vero.', 'Y', 19, 'articles', '2017-08-18 11:25:55', '2018-08-04 14:41:59', NULL),
(111, 4, 'Voluptatem omnis accusantium placeat ipsum repudiandae sit. Tempora et ut et quibusdam tempore dolores. Sint sed quia labore eius. Omnis quasi illum aut.', 'Y', 19, 'articles', '2017-08-05 10:04:40', '2018-08-04 14:41:59', NULL),
(112, 3, 'Enim commodi deleniti sapiente voluptatibus nihil inventore amet. Sed ea voluptatem possimus inventore quidem omnis animi ipsam. Qui tempore deleniti non numquam.', 'Y', 20, 'articles', '2018-01-08 00:13:43', '2018-08-04 14:41:59', NULL),
(113, 1, 'Hic suscipit unde provident occaecati perferendis. Temporibus inventore voluptatem laboriosam ea. Officiis sint dolores enim occaecati omnis. Consequatur sunt quod magni quaerat voluptatem laborum voluptatum.', 'Y', 20, 'articles', '2018-02-20 02:56:34', '2018-08-04 14:41:59', NULL),
(114, 5, 'Animi iusto quis illum rerum in natus rerum. Sed harum eligendi aspernatur porro. Fugit dolorem nostrum itaque tenetur quibusdam facere est. Ex voluptatem enim rerum mollitia omnis a.', 'Y', 20, 'articles', '2017-08-15 09:50:15', '2018-08-04 14:41:59', NULL),
(115, 5, 'Quasi in quia debitis dicta blanditiis quo. Voluptatem quia autem et magni nisi ratione. Inventore id eligendi earum vel et est.', 'Y', 20, 'articles', '2017-10-01 16:44:40', '2018-08-04 14:41:59', NULL),
(116, 5, 'Voluptas omnis culpa aut sed expedita odio recusandae. Voluptatem eveniet saepe est itaque facere dicta.', 'Y', 20, 'articles', '2018-06-12 11:57:39', '2018-08-04 14:41:59', NULL),
(117, 2, 'Et quod commodi ratione ut. Consequatur eaque qui sequi aperiam modi omnis. Aut sequi sit et accusantium perspiciatis est dicta. Molestiae omnis ratione assumenda modi ut excepturi architecto.', 'Y', 20, 'articles', '2018-07-19 16:07:17', '2018-08-04 14:41:59', NULL),
(118, 1, 'Assumenda doloremque et qui ea. At dolorum cum quis error qui. Facere suscipit magni aut. Ducimus magni neque ut provident dolore ut. Molestias necessitatibus vel optio rem odit et.', 'Y', 21, 'articles', '2018-06-05 13:01:53', '2018-08-04 14:41:59', NULL),
(119, 1, 'In dignissimos quia quis repellat consequatur dolorem. Voluptas excepturi a aut quis qui. Eum incidunt corrupti ratione et. Laboriosam repellat eius commodi qui. Et nemo porro tempora et omnis id tempore.', 'Y', 21, 'articles', '2017-08-24 04:53:16', '2018-08-04 14:41:59', NULL),
(120, 5, 'Dolorem est consequatur qui aspernatur. Consequatur ducimus incidunt incidunt molestiae ad illum ut.', 'Y', 21, 'articles', '2017-09-11 16:09:08', '2018-08-04 14:41:59', NULL),
(121, 3, 'Et nesciunt quaerat eum odit dolor eaque. Provident unde ex molestias excepturi. Consequatur minus et sint optio.', 'Y', 21, 'articles', '2017-12-31 13:20:28', '2018-08-04 14:41:59', NULL),
(122, 4, 'Est aut sint eius pariatur esse. Corporis dolorum dolor repellendus quia. Accusamus aut omnis qui. Qui delectus expedita voluptatem pariatur aliquam non qui.', 'Y', 22, 'articles', '2018-06-07 03:49:29', '2018-08-04 14:41:59', NULL),
(123, 2, 'Minus optio nemo exercitationem officia. Sint qui rerum omnis esse. Architecto veniam eveniet sapiente non voluptate iste optio placeat. Nisi assumenda laboriosam ratione aut velit rerum quia.', 'Y', 22, 'articles', '2018-04-08 12:38:52', '2018-08-04 14:41:59', NULL),
(124, 2, 'Blanditiis quas ea eum aut. Omnis cumque ut blanditiis corporis dolor atque. Voluptas molestias odit consectetur ut commodi. Nemo quibusdam quia delectus iure ullam.', 'Y', 22, 'articles', '2017-11-07 07:59:31', '2018-08-04 14:41:59', NULL),
(125, 3, 'Repellendus reprehenderit voluptatem laudantium ipsum. Quibusdam deleniti velit delectus voluptatibus corporis. Iste magnam cupiditate tempore porro odit eveniet.', 'Y', 22, 'articles', '2018-03-23 04:20:05', '2018-08-04 14:41:59', NULL),
(126, 2, 'Dolorem natus iusto necessitatibus enim. Eum qui harum officiis quidem debitis. Et et occaecati nobis occaecati quia et. Modi ut dolore qui tenetur molestiae voluptas.', 'Y', 22, 'articles', '2017-09-08 22:55:11', '2018-08-04 14:41:59', NULL),
(127, 1, 'Culpa architecto quisquam minima non eos maxime. Odit perspiciatis soluta minima autem et quis aliquam et. Commodi mollitia labore nobis quae.', 'Y', 23, 'articles', '2018-05-15 04:16:59', '2018-08-04 14:41:59', NULL),
(128, 3, 'Repellendus fuga doloribus nisi quod cupiditate. Ut blanditiis reiciendis velit ex. Quis quia nihil perspiciatis consequatur sed sequi laborum voluptatem.', 'Y', 23, 'articles', '2018-05-14 09:33:57', '2018-08-04 14:41:59', NULL),
(129, 4, 'Et impedit et ut est. Adipisci unde sed quas ab nostrum porro distinctio. Rerum omnis est nemo mollitia molestias velit qui.', 'Y', 23, 'articles', '2018-04-16 11:50:06', '2018-08-04 14:41:59', NULL),
(130, 1, 'Quisquam dignissimos hic qui ut nesciunt eius. Commodi nisi qui natus et voluptate incidunt id nostrum. Excepturi reiciendis consequatur iste ut qui quidem quos. Ea hic necessitatibus ut sit provident et.', 'Y', 23, 'articles', '2018-05-01 04:24:17', '2018-08-04 14:41:59', NULL),
(131, 4, 'Et eum ipsum dolorem earum repudiandae hic. Odio est voluptatem id. Pariatur voluptatum et dolor dolores praesentium dolores explicabo non.', 'Y', 23, 'articles', '2017-10-27 04:59:59', '2018-08-04 14:41:59', NULL),
(132, 1, 'Sunt tempora dolorem odio. Ratione aliquam dolor sit vitae. Ipsa est qui et aut qui in. Nisi et minus sit consequatur. Beatae nam at sit. Temporibus et corporis qui hic ducimus. Voluptate rerum totam eius nihil.', 'Y', 24, 'articles', '2017-09-11 05:54:14', '2018-08-04 14:41:59', NULL),
(133, 1, 'Facere et impedit totam voluptatem impedit. Labore eius sit non voluptate ad. Consectetur laboriosam nisi doloribus in. Aperiam quidem dolor iusto non autem aut. Unde tempore praesentium aut saepe.', 'Y', 24, 'articles', '2017-09-05 11:41:48', '2018-08-04 14:41:59', NULL),
(134, 2, 'Assumenda in incidunt qui. Dolorem vitae officiis nemo repellendus nobis recusandae. Aut eveniet et distinctio repellat enim sed alias. Error corrupti a distinctio.', 'Y', 24, 'articles', '2017-11-03 04:41:44', '2018-08-04 14:41:59', NULL),
(135, 3, 'Esse nobis aut sed assumenda iusto dolor impedit. Velit illo sint molestias vel. Unde laboriosam quia maxime facilis. Tempora nisi qui ea blanditiis vel. Dolores nihil dignissimos aspernatur adipisci aut blanditiis.', 'Y', 24, 'articles', '2017-10-11 18:55:06', '2018-08-04 14:41:59', NULL),
(136, 5, 'Vero aut ut consequatur nihil ut sed dolorem. Voluptatibus et officia natus dolorem. Delectus ut eum eius corrupti. Sunt minus placeat illum veniam voluptas aut. Aut et animi veritatis.', 'Y', 24, 'articles', '2018-03-13 22:31:10', '2018-08-04 14:41:59', NULL),
(137, 3, 'Quia omnis unde accusantium aut dolorem et nulla. Et reiciendis laborum nulla. Tenetur explicabo assumenda ut delectus sed. Sit vel sed omnis sit adipisci et.', 'Y', 25, 'articles', '2017-12-24 19:53:44', '2018-08-04 14:41:59', NULL),
(138, 4, 'Et placeat illo in et et omnis. Praesentium recusandae aut explicabo pariatur. Eum explicabo in at culpa unde sed maiores.', 'Y', 25, 'articles', '2018-04-17 13:16:26', '2018-08-04 14:41:59', NULL),
(139, 3, 'Et facilis velit molestiae sit dolorum qui corporis. Pariatur aut impedit qui perspiciatis earum sint a. Et hic nostrum alias aspernatur aut voluptatem molestiae.', 'Y', 25, 'articles', '2017-11-26 04:18:02', '2018-08-04 14:41:59', NULL),
(140, 2, 'Voluptatibus dicta qui quia consequuntur consequuntur aut explicabo. Aliquid est dignissimos dolor ut in illum et. Voluptatem praesentium dolorem quo numquam deleniti blanditiis.', 'Y', 1, 'videos', '2017-08-10 01:15:58', '2018-08-04 14:41:59', NULL),
(141, 2, 'Quidem vero accusamus eum placeat id a id. Modi ducimus aut dolor qui facilis. Repellat consequatur accusamus assumenda.', 'Y', 1, 'videos', '2018-05-27 21:40:15', '2018-08-04 14:41:59', NULL),
(142, 1, 'Quis explicabo veritatis unde voluptatibus ut vel est. Dignissimos voluptas deleniti qui iste eos. Delectus perferendis aliquid sit. Nostrum ut at voluptas molestias cupiditate quibusdam numquam.', 'Y', 1, 'videos', '2018-01-22 21:59:14', '2018-08-04 14:41:59', NULL),
(143, 1, 'Voluptatem ipsam natus iure voluptas sapiente saepe eveniet debitis. At fuga molestiae eum ut aut odit. Et quas nisi eius voluptatibus odit est. Delectus qui voluptate sint dolor fuga iste numquam.', 'Y', 1, 'videos', '2018-07-17 20:11:57', '2018-08-04 14:41:59', NULL),
(144, 5, 'A ea magni voluptatem ut unde magnam ea exercitationem. Voluptatem iusto et sit eum excepturi aut. Ut expedita est sequi autem. Qui incidunt a nulla dolorem debitis.', 'Y', 1, 'videos', '2018-03-14 02:54:36', '2018-08-04 14:41:59', NULL),
(145, 5, 'Impedit quasi labore debitis laborum maxime ipsam. Ipsum porro culpa dicta sed. Libero et dignissimos soluta tempore. Eos delectus ducimus quam expedita.', 'Y', 1, 'videos', '2017-12-10 12:17:00', '2018-08-04 14:41:59', NULL),
(146, 5, 'Provident harum velit sit et laudantium inventore. Reprehenderit quo aut ex hic eos ipsam atque. Qui voluptas illo quod. Et recusandae et beatae id.', 'Y', 1, 'videos', '2018-06-25 20:41:50', '2018-08-04 14:41:59', NULL),
(147, 1, 'Rerum aut molestias sit autem. Natus molestiae doloremque et et nesciunt. Molestiae quod non dolorem exercitationem ipsa magnam totam. Exercitationem quae quis modi voluptatibus deleniti.', 'Y', 2, 'videos', '2017-08-11 08:08:25', '2018-08-04 14:41:59', NULL),
(148, 2, 'Dolorem amet cupiditate omnis deleniti consectetur quia. Et aperiam rem excepturi qui vitae harum et. Eos architecto nemo dolore aperiam sunt et voluptas. Sunt omnis rem dolore non quidem.', 'Y', 2, 'videos', '2018-05-21 15:11:57', '2018-08-04 14:42:00', NULL),
(149, 4, 'Assumenda labore error distinctio fugiat et quis. Et quia sed nobis ex consequuntur est ipsum. Mollitia quis recusandae nisi tempora consequatur. Magnam molestias et dolore.', 'Y', 2, 'videos', '2018-01-07 15:23:16', '2018-08-04 14:42:00', NULL),
(150, 5, 'Dolorum voluptatem totam et. Sed error dolorem veniam animi sequi id delectus.', 'Y', 2, 'videos', '2018-08-01 20:18:49', '2018-08-04 14:42:00', NULL),
(151, 1, 'Enim ab fuga aut dolorem occaecati quidem. Natus eos saepe itaque similique quo unde. Nihil et ipsum ipsam in reprehenderit quos.', 'Y', 2, 'videos', '2018-06-28 14:19:15', '2018-08-04 14:42:00', NULL),
(152, 2, 'Dolor iste ea ratione qui. Qui officia et dolor optio optio sunt. Eius dolorem et aut rem exercitationem delectus. Et voluptates totam perferendis architecto.', 'Y', 2, 'videos', '2018-05-21 09:58:08', '2018-08-04 14:42:00', NULL),
(153, 2, 'Excepturi voluptatem est et velit libero totam velit. Sit ut perspiciatis accusantium sed laudantium. Alias sequi dolores odio sint.', 'Y', 2, 'videos', '2017-08-19 21:14:13', '2018-08-04 14:42:00', NULL),
(154, 3, 'Veniam possimus quia sed necessitatibus. Molestias doloremque dolorum aperiam ipsa exercitationem sint dolorem. Id quo numquam temporibus et.', 'Y', 3, 'videos', '2018-01-06 13:35:38', '2018-08-04 14:42:00', NULL),
(155, 2, 'Qui facere rerum aspernatur rerum. Occaecati voluptas fugit consectetur architecto non quis nobis. Corrupti laboriosam omnis at alias.', 'Y', 3, 'videos', '2018-04-17 06:34:28', '2018-08-04 14:42:00', NULL),
(156, 4, 'Quia omnis ipsam quidem quibusdam voluptatibus sint perferendis. Incidunt amet repellat ut debitis rerum totam aut pariatur. Maiores dignissimos veniam assumenda temporibus dolores voluptatem error.', 'Y', 3, 'videos', '2017-08-24 15:59:51', '2018-08-04 14:42:00', NULL),
(157, 4, 'Quis est voluptates voluptates. Quia corrupti sed ut voluptatem provident aliquid natus. Magnam dolor consequatur nihil quidem. Dignissimos reprehenderit veritatis veniam.', 'Y', 4, 'videos', '2017-12-21 18:10:37', '2018-08-04 14:42:00', NULL),
(158, 5, 'Eligendi placeat blanditiis sequi sint neque. Assumenda adipisci magnam quo ducimus quasi ipsam. Cum perspiciatis nobis saepe voluptas dolores sed.', 'Y', 4, 'videos', '2018-02-18 06:59:38', '2018-08-04 14:42:00', NULL),
(159, 5, 'Sed libero sapiente voluptas sunt illum quia ipsa. Veniam aliquid sed velit veniam facere aspernatur. Sed cumque earum impedit est officia et. Eos reiciendis corporis nam repellendus animi inventore debitis.', 'Y', 5, 'videos', '2017-12-02 04:08:49', '2018-08-04 14:42:00', NULL),
(160, 2, 'Vel inventore libero qui a suscipit. Voluptas eius ad vero et aut quod quod. Illo labore delectus commodi. Minima qui et rerum.', 'Y', 5, 'videos', '2018-06-15 20:09:14', '2018-08-04 14:42:00', NULL),
(161, 3, 'Ea quia explicabo magnam autem voluptas enim. Nihil earum aperiam perferendis odit voluptatibus illum soluta. In enim laborum aliquid dolorem. Occaecati soluta voluptatibus id est aut voluptas necessitatibus veritatis.', 'Y', 5, 'videos', '2018-07-25 23:38:27', '2018-08-04 14:42:00', NULL),
(162, 1, 'Rerum laudantium recusandae dolorem laudantium quam sed. Dolores quasi doloremque minima odio et rerum numquam. Cupiditate et architecto omnis ut aut.', 'Y', 5, 'videos', '2018-06-02 11:07:41', '2018-08-04 14:42:00', NULL),
(163, 4, 'Tempore dignissimos eum eveniet omnis sit autem odit. Vitae quisquam praesentium perspiciatis sit aut. Sapiente fuga magnam quis eos et eligendi nihil et. Inventore rem molestias nihil provident placeat rerum.', 'Y', 5, 'videos', '2018-05-15 17:20:59', '2018-08-04 14:42:00', NULL),
(164, 2, 'Aut perferendis expedita praesentium et. Fugit non culpa dolores exercitationem non ab dolores. Commodi temporibus architecto autem quam sit vitae.', 'Y', 6, 'videos', '2018-06-11 17:15:35', '2018-08-04 14:42:00', NULL),
(165, 3, 'Consectetur fuga consequuntur porro autem. Consequatur nulla quo delectus ea omnis illum. Hic ut consequatur minus. Facilis nulla recusandae sit earum minus et laudantium et.', 'Y', 6, 'videos', '2017-10-24 10:18:51', '2018-08-04 14:42:00', NULL),
(166, 4, 'Laudantium quae est tenetur vel magni. Enim autem adipisci quo voluptas sint cum eos. Sit aliquam omnis excepturi ratione voluptatem maiores sunt.', 'Y', 6, 'videos', '2018-02-15 12:40:30', '2018-08-04 14:42:00', NULL),
(167, 4, 'Facilis est omnis amet nostrum corrupti harum. Eligendi et et quidem voluptas harum quia unde. Quis autem at dolores.', 'Y', 6, 'videos', '2017-08-15 03:09:51', '2018-08-04 14:42:00', NULL),
(168, 3, 'Ullam quia sit quis deleniti. Voluptas laborum rerum blanditiis ea non voluptas. Voluptatem aspernatur est non ipsum qui quia. Facilis consequatur asperiores eaque ullam dolor.', 'Y', 6, 'videos', '2018-03-30 12:56:13', '2018-08-04 14:42:00', NULL),
(169, 2, 'Nam et autem aut esse et. Labore repudiandae quia quasi voluptate ut. Non qui iure qui dolorem laborum qui velit.', 'Y', 6, 'videos', '2017-12-16 01:54:02', '2018-08-04 14:42:00', NULL),
(170, 2, 'Est autem aliquid fugiat distinctio. Ut delectus quidem qui deleniti reiciendis. Molestias dolor sit distinctio vel laudantium tempora. Voluptas autem necessitatibus placeat quia repudiandae hic.', 'Y', 6, 'videos', '2018-07-02 11:58:38', '2018-08-04 14:42:00', NULL),
(171, 2, 'Ullam ut tempore et et esse. Qui adipisci delectus aut corporis totam excepturi quidem rerum.', 'Y', 7, 'videos', '2017-11-14 04:26:22', '2018-08-04 14:42:00', NULL),
(172, 5, 'Unde laudantium rerum minima neque dicta ipsum. Expedita ut voluptatem aut vero.', 'Y', 7, 'videos', '2018-01-18 07:36:39', '2018-08-04 14:42:00', NULL),
(173, 3, 'Eveniet vel totam doloremque earum non. Laudantium enim est ipsam dolor. Est nobis dolore ea harum quia.', 'Y', 7, 'videos', '2017-12-24 17:52:31', '2018-08-04 14:42:00', NULL),
(174, 3, 'Sed officiis quo reiciendis esse autem sit. Ea nihil nisi et qui ipsum. Perspiciatis eos maxime necessitatibus et impedit dolores laboriosam quaerat. Ut voluptatem mollitia aut ad.', 'Y', 7, 'videos', '2017-09-16 14:18:38', '2018-08-04 14:42:00', NULL),
(175, 2, 'Est in consequatur veritatis voluptate nam. Hic explicabo totam ea fugit tempora. Blanditiis nulla at sint velit saepe nostrum veniam.', 'Y', 7, 'videos', '2017-10-09 12:49:15', '2018-08-04 14:42:00', NULL),
(176, 4, 'Omnis quo qui aliquam aperiam tempora. Eos ipsa iusto delectus. Reiciendis quia excepturi eos est.', 'Y', 7, 'videos', '2018-01-01 16:06:55', '2018-08-04 14:42:00', NULL),
(177, 4, 'Repellendus veniam sequi id. Nihil sint pariatur aperiam dolorum accusamus velit. Sit facilis fuga fugit sapiente nulla illum quis.', 'Y', 7, 'videos', '2017-10-14 22:00:00', '2018-08-04 14:42:00', NULL),
(178, 3, 'Sunt quam et et ullam voluptatibus soluta totam. Tempora quas quidem omnis porro iure.', 'Y', 8, 'videos', '2018-07-04 23:38:45', '2018-08-04 14:42:00', NULL),
(179, 1, 'Quia nobis deleniti rerum ipsum ut qui. Consequuntur molestias dignissimos dolorum et perspiciatis. Natus amet labore aut suscipit provident. Velit officiis harum excepturi sunt dolorem fuga.', 'Y', 8, 'videos', '2017-09-11 06:25:29', '2018-08-04 14:42:00', NULL),
(180, 1, 'Nisi ipsam nesciunt voluptas possimus laudantium debitis non sint. Consequuntur quisquam voluptas perspiciatis libero architecto. Dolorem rerum consequuntur et suscipit quia et consectetur accusamus.', 'Y', 8, 'videos', '2018-02-05 01:05:23', '2018-08-04 14:42:00', NULL),
(181, 5, 'Dolorem natus dicta rerum ut aliquid eos aliquam. Voluptate cumque doloremque corrupti quia. Dolor possimus doloribus voluptas libero.', 'Y', 8, 'videos', '2018-02-03 04:21:27', '2018-08-04 14:42:00', NULL),
(182, 1, 'Ratione et et hic numquam. Expedita et deleniti necessitatibus vero excepturi. Quo itaque sint sint doloremque accusantium quis dolorum. Praesentium quasi cupiditate voluptatibus voluptatem corrupti est modi.', 'Y', 8, 'videos', '2017-12-09 17:13:17', '2018-08-04 14:42:01', NULL),
(183, 4, 'Mollitia distinctio iure nam. Atque et neque quia deleniti. Alias rerum aliquid recusandae est. Voluptatibus ipsam aut quod iste. Harum et eum est soluta quam. Cumque qui accusamus dolor. Est doloremque minima facere.', 'Y', 8, 'videos', '2018-08-03 18:39:30', '2018-08-04 14:42:01', NULL),
(184, 3, 'Laudantium eveniet quas alias dolorem officiis sapiente. Dolore ratione suscipit illum cum. Quidem asperiores ad nihil.', 'Y', 8, 'videos', '2018-05-19 04:05:47', '2018-08-04 14:42:01', NULL),
(185, 2, 'Tempora sit a repellat iste occaecati. Error alias qui et et ut itaque veniam. Error et ab dignissimos qui odit quasi. Natus et est nisi.', 'Y', 8, 'videos', '2017-10-15 02:29:29', '2018-08-04 14:42:01', NULL),
(186, 1, 'Et enim et alias deleniti enim dolorum rerum. Officiis omnis in sint hic aut mollitia. Suscipit est dolore et beatae facere dolorem quia. Sint voluptatem ut cupiditate eos cupiditate voluptas voluptas eos.', 'Y', 9, 'videos', '2018-03-26 16:21:31', '2018-08-04 14:42:01', NULL),
(187, 4, 'Quia ut repudiandae in provident perferendis tempore. Ipsam id ex corporis rerum. Temporibus eos excepturi tenetur quasi similique veniam sunt. Amet veniam delectus maxime.', 'Y', 9, 'videos', '2018-04-10 14:04:19', '2018-08-04 14:42:01', NULL),
(188, 1, 'Rem vitae quia aut tempora. Qui odit magnam quisquam eius qui est. Ut quis asperiores sit eaque ut eum. Ut libero esse qui aperiam vitae eaque distinctio culpa.', 'Y', 9, 'videos', '2018-03-03 00:32:09', '2018-08-04 14:42:01', NULL),
(189, 2, 'Odit dolores et enim fugit. Architecto adipisci error qui dolores. Voluptas et et nostrum qui dolorem. Sed sed id esse eum.', 'Y', 9, 'videos', '2017-08-07 05:45:41', '2018-08-04 14:42:01', NULL),
(190, 3, 'Qui repellendus impedit repellendus incidunt reiciendis. Ratione laboriosam nesciunt eum eaque sed. Corporis porro exercitationem nostrum et laudantium dolore. Est atque dolores a sequi dolores minima pariatur.', 'Y', 9, 'videos', '2018-03-04 22:16:10', '2018-08-04 14:42:01', NULL),
(191, 4, 'Dolorem magnam suscipit ut ut id est. Cupiditate eos corporis necessitatibus autem cumque quo.', 'Y', 10, 'videos', '2018-06-25 00:26:13', '2018-08-04 14:42:01', NULL),
(192, 5, 'Quidem neque nihil vel saepe atque nihil. Optio amet et magnam architecto. Et ipsam dolor laudantium ab. Beatae exercitationem aut sint aut eligendi minus dolorem.', 'Y', 10, 'videos', '2017-09-01 18:54:35', '2018-08-04 14:42:01', NULL),
(193, 2, 'Voluptates similique quisquam quia illum ducimus. Ipsa ut voluptatibus doloremque dolor repellat dolores maxime. Odio eum sint nobis quidem saepe odit. Quasi nam et error commodi.', 'Y', 10, 'videos', '2017-11-28 14:15:40', '2018-08-04 14:42:01', NULL),
(194, 3, 'Mollitia sapiente iure sint quae dignissimos. Nobis qui veritatis sed incidunt. Eos velit voluptas consequuntur iusto molestiae nihil est.', 'Y', 10, 'videos', '2017-09-22 22:48:11', '2018-08-04 14:42:01', NULL),
(195, 5, 'Incidunt facilis quaerat reiciendis nobis saepe est ipsum. Omnis mollitia qui amet maiores quidem fugit. Id blanditiis rem mollitia assumenda est. Eius sit ut qui quis rem. Aut temporibus rem neque aut a vel.', 'Y', 10, 'videos', '2017-10-21 09:56:22', '2018-08-04 14:42:01', NULL),
(196, 4, 'Ducimus qui saepe animi fugit. Debitis explicabo dignissimos cumque iure aut. Vitae vel explicabo nihil sit sint consequatur harum.', 'Y', 10, 'videos', '2018-02-24 06:50:00', '2018-08-04 14:42:01', NULL),
(197, 5, 'Doloribus reprehenderit omnis nam soluta omnis. Illum architecto ipsam autem rerum laborum voluptatem autem impedit. Quis culpa dolorum consequatur ab assumenda est voluptas.', 'Y', 10, 'videos', '2018-03-21 13:19:37', '2018-08-04 14:42:01', NULL),
(198, 1, 'Facilis ut amet temporibus voluptas nesciunt. Repudiandae ut accusantium numquam consequuntur cumque harum. Quo odio rerum voluptatem est voluptatem ipsam aspernatur quia.', 'Y', 11, 'videos', '2017-08-25 06:43:24', '2018-08-04 14:42:01', NULL),
(199, 5, 'Modi expedita consectetur velit. Eos ratione distinctio debitis cumque et laboriosam delectus. Ratione aut hic cupiditate dolorum nisi fuga.', 'Y', 11, 'videos', '2017-11-14 04:10:00', '2018-08-04 14:42:01', NULL),
(200, 2, 'Blanditiis ad eligendi aliquam aspernatur et. Maiores omnis dolores officiis illo nisi. Veritatis sit eaque et molestias ut aperiam aut et.', 'Y', 11, 'videos', '2017-12-15 20:26:37', '2018-08-04 14:42:01', NULL),
(201, 5, 'Repellendus eos quasi natus voluptas non. Iste in in eum et. Ad velit est sit deserunt ipsam repudiandae veniam. Maxime optio et maxime ipsam quis.', 'Y', 11, 'videos', '2018-02-28 14:55:08', '2018-08-04 14:42:01', NULL),
(202, 3, 'Minima rerum est illo aut. Et aspernatur molestiae quia aut vel illo magnam officia. Nobis consectetur cumque molestiae esse accusantium.', 'Y', 11, 'videos', '2018-04-21 07:39:19', '2018-08-04 14:42:01', NULL),
(203, 1, 'Vero aspernatur id illo fugiat sequi impedit nobis perspiciatis. Qui suscipit aliquam qui et voluptas. Veritatis repellat natus rerum et esse.', 'Y', 11, 'videos', '2017-11-14 05:57:54', '2018-08-04 14:42:01', NULL),
(204, 2, 'Velit commodi blanditiis voluptate sit. Porro quod accusantium laboriosam est. Veritatis sed veritatis debitis id est quos.', 'Y', 12, 'videos', '2018-07-23 21:31:33', '2018-08-04 14:42:01', NULL),
(205, 2, 'Omnis quia magni nisi voluptatibus sit molestiae. Ipsam et id ullam tenetur iste. Aut sequi nihil dolorem fuga in. In debitis qui ex blanditiis accusantium est quo quia.', 'Y', 12, 'videos', '2018-01-26 17:18:13', '2018-08-04 14:42:01', NULL),
(206, 3, 'Esse possimus dolorem sit autem. Accusantium debitis error necessitatibus id quis. Ut est eaque quisquam eos vero dicta nihil odit.', 'Y', 12, 'videos', '2017-10-31 11:26:55', '2018-08-04 14:42:01', NULL),
(207, 1, 'Velit qui voluptas nihil officia quasi est consectetur. Est similique sed voluptatibus odit. Sint doloribus aut consectetur aut eaque sed aut nobis.', 'Y', 12, 'videos', '2018-02-07 07:12:40', '2018-08-04 14:42:01', NULL),
(208, 3, 'Possimus nam qui corrupti aspernatur. Iusto ratione numquam id molestiae possimus incidunt dolore. Consequatur nulla laboriosam nemo repudiandae natus ipsa rerum.', 'Y', 12, 'videos', '2018-07-26 12:46:26', '2018-08-04 14:42:01', NULL),
(209, 1, 'Hic labore voluptatem pariatur commodi quos voluptas hic. Quis deleniti libero sit ut sed vero quia. Recusandae eaque temporibus esse vitae quis ut consequatur ab. Quos vel et odio dolorum iste tempore vel.', 'Y', 12, 'videos', '2018-02-19 01:10:16', '2018-08-04 14:42:01', NULL);
INSERT INTO `comments` (`id`, `user_id`, `details`, `display`, `commentable_id`, `commentable_type`, `created_at`, `updated_at`, `deleted_at`) VALUES
(210, 5, 'Odio error sit aut dolorum voluptatem blanditiis dolorem. At et sit labore officia laudantium sequi voluptatum. Repudiandae saepe enim eum distinctio aut qui aut. Id alias velit ut ipsam.', 'Y', 13, 'videos', '2017-10-05 17:19:42', '2018-08-04 14:42:01', NULL),
(211, 1, 'Voluptatibus quasi optio aut sint sed amet. Amet voluptas quos mollitia expedita. Voluptas enim eveniet fugit aliquam fuga eius. Expedita voluptatibus reprehenderit pariatur suscipit rem fuga facere.', 'Y', 13, 'videos', '2017-09-11 23:25:58', '2018-08-04 14:42:01', NULL),
(212, 4, 'Eum et ut possimus excepturi. Sed qui voluptatem illo est dolores. Repellat placeat magnam et qui iste. Velit sapiente dolor quibusdam vitae. Illo nemo pariatur quis molestiae dolorem in. Non necessitatibus ut a et ut.', 'Y', 13, 'videos', '2018-07-01 16:30:46', '2018-08-04 14:42:01', NULL),
(213, 3, 'Et laborum asperiores qui nihil odio asperiores. Quasi fugiat et velit consequuntur. Cumque enim non temporibus debitis quia delectus. Qui rem blanditiis quis sint ea.', 'Y', 13, 'videos', '2018-07-26 04:36:50', '2018-08-04 14:42:01', NULL),
(214, 3, 'Dolor quaerat fugit quasi blanditiis. Qui labore rerum sint suscipit perspiciatis nemo fugit quia. Quo ducimus molestiae sit quis impedit.', 'Y', 14, 'videos', '2017-09-02 15:48:10', '2018-08-04 14:42:01', NULL),
(215, 5, 'Omnis quos dolorem ipsam sequi. Nobis dolorem tempore dolores. Nulla nulla veniam repudiandae.', 'Y', 14, 'videos', '2018-02-27 08:55:38', '2018-08-04 14:42:01', NULL),
(216, 2, 'Et perferendis asperiores id in at. Architecto id dolorem non perspiciatis in laudantium. Rerum ut et error fugiat id doloribus. Voluptas iste soluta nemo facere quia est.', 'Y', 14, 'videos', '2017-10-13 05:03:33', '2018-08-04 14:42:01', NULL),
(217, 1, 'Sapiente eos nesciunt aliquam nihil non aut hic. Earum est enim alias eos deleniti officia autem et. Molestiae a quae eaque ullam fugit et.', 'Y', 14, 'videos', '2018-02-14 05:26:08', '2018-08-04 14:42:01', NULL),
(218, 3, 'Optio et vero corrupti perferendis veritatis hic neque. Dolorum dolorem error error aut est accusantium. Mollitia est nam sed consequatur velit quisquam nihil reprehenderit.', 'Y', 14, 'videos', '2018-05-08 14:36:20', '2018-08-04 14:42:01', NULL),
(219, 4, 'Quis a sit reprehenderit fugit. Voluptatem quos deleniti voluptatibus earum dolor velit. Dolor id est libero cumque.', 'Y', 14, 'videos', '2017-08-30 15:36:01', '2018-08-04 14:42:01', NULL),
(220, 1, 'Modi possimus dolorem pariatur harum labore. Aliquid quis qui rerum iure. Voluptatem quo consequatur sapiente culpa earum eos omnis. Dolorem cupiditate voluptates minima quam et. Dolor rerum est omnis fugiat eum.', 'Y', 14, 'videos', '2018-03-16 06:07:16', '2018-08-04 14:42:02', NULL),
(221, 1, 'In iure qui id reiciendis quod. Hic sed in debitis beatae nulla atque non. Quas eaque sequi et quas perferendis voluptatem.', 'Y', 14, 'videos', '2018-01-22 23:23:42', '2018-08-04 14:42:02', NULL),
(222, 5, 'Occaecati repudiandae ut possimus. Ea aut voluptates consequuntur ut modi est iste.', 'Y', 15, 'videos', '2018-04-18 18:29:43', '2018-08-04 14:42:02', NULL),
(223, 5, 'Ratione laudantium accusantium dolor neque et. Quaerat repellat ut nemo accusantium iste hic. Repellendus quibusdam quas rerum atque. Et consequuntur tempora ut tenetur ab. Aut cumque mollitia consequuntur quia sed aut.', 'Y', 15, 'videos', '2017-08-04 18:54:10', '2018-08-04 14:42:02', NULL),
(224, 4, 'Possimus repudiandae et ut facere. Et eveniet voluptatem aut autem. Aut placeat velit maiores.', 'Y', 15, 'videos', '2017-08-19 12:10:11', '2018-08-04 14:42:02', NULL),
(225, 1, 'Est praesentium nobis in repudiandae veritatis molestiae dignissimos. In eos corporis dolor velit velit. Aut eum aut quia. Ullam vel nulla nam et sunt.', 'Y', 15, 'videos', '2018-05-31 05:15:26', '2018-08-04 14:42:02', NULL),
(226, 3, 'Cumque asperiores voluptates rerum tenetur quia unde sed rerum. Amet ab tempora iusto maiores. Repellendus ullam sed saepe fuga dolor blanditiis nisi possimus. Corporis earum enim sed ducimus minus ut quasi.', 'Y', 15, 'videos', '2017-12-27 03:19:37', '2018-08-04 14:42:02', NULL),
(227, 3, 'Quam animi est rem officia. Doloremque suscipit ab id quasi ut.', 'Y', 15, 'videos', '2017-11-24 08:18:26', '2018-08-04 14:42:02', NULL),
(228, 4, 'Omnis ad consequatur facere rem excepturi. Qui rem et suscipit quia. Eum quia incidunt consectetur et eaque sed laborum autem.', 'Y', 16, 'videos', '2017-09-11 00:33:07', '2018-08-04 14:42:02', NULL),
(229, 5, 'Quidem quia mollitia iste et aut qui vel. Accusantium nihil adipisci sit sequi incidunt nihil. Voluptates nesciunt repudiandae doloribus modi assumenda facilis illum. Ratione sint velit quia voluptatem.', 'Y', 16, 'videos', '2017-08-26 04:00:59', '2018-08-04 14:42:02', NULL),
(230, 5, 'Corporis ad repudiandae officiis aut aliquam delectus. Est et ducimus non molestiae distinctio. Est atque officiis quia aspernatur non. Laudantium asperiores aut aut rem perferendis.', 'Y', 17, 'videos', '2018-04-25 04:05:11', '2018-08-04 14:42:02', NULL),
(231, 1, 'Dolor quasi et iste cumque. Neque voluptatibus impedit est reiciendis quis itaque voluptatem. Labore amet quos autem dignissimos. Illum sapiente placeat fugiat ab. Nobis omnis exercitationem quis et et.', 'Y', 17, 'videos', '2018-05-31 08:43:51', '2018-08-04 14:42:02', NULL),
(232, 2, 'Eum fugit voluptatem molestias sunt cupiditate qui quisquam. Consectetur aut ab excepturi occaecati porro. Porro laborum ut distinctio eum. Omnis qui culpa reiciendis et doloremque.', 'Y', 17, 'videos', '2018-08-02 09:25:53', '2018-08-04 14:42:02', NULL),
(233, 2, 'Illo et velit veritatis exercitationem commodi et. Ea ea rem voluptas animi deleniti. Cupiditate architecto sint eveniet dolorum.', 'Y', 17, 'videos', '2018-04-30 06:12:20', '2018-08-04 14:42:02', NULL),
(234, 4, 'Veritatis voluptatem vitae ut occaecati suscipit ad nihil. Omnis adipisci sint enim quidem mollitia aut. Alias non iure inventore cupiditate illo.', 'Y', 17, 'videos', '2018-04-24 08:24:09', '2018-08-04 14:42:02', NULL),
(235, 4, 'Ratione nesciunt itaque odit sit praesentium assumenda. Et veniam id quas quis ratione architecto et aut. Dolore officia dolorum consectetur at. Excepturi cupiditate aut quia quas.', 'Y', 17, 'videos', '2018-04-03 03:16:18', '2018-08-04 14:42:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE IF NOT EXISTS `galleries` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `display` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `description`, `display`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Programming', 'Sint corporis architecto itaque et ducimus. Possimus occaecati aut et molestiae placeat sit velit. Eum aut aspernatur veniam cum sint fugit recusandae.', 'Y', '2018-03-10 16:21:20', '2018-07-25 10:01:56', NULL),
(2, 'Science', 'Quia sapiente rem doloribus dolorum ex. Voluptatem aspernatur vero iure tempora placeat.', 'Y', '2018-06-23 02:21:44', '2018-08-02 18:00:21', NULL),
(3, 'Bangladesh', 'Qui atque voluptas explicabo nisi quam. Aspernatur autem aut sunt dignissimos fugit consequatur.', 'Y', '2018-07-15 13:05:25', '2018-07-14 06:28:01', NULL),
(4, 'Motivational', 'Corrupti assumenda iusto ut voluptas. Ducimus velit temporibus nam sit velit velit sit.', 'Y', '2017-12-31 10:48:14', '2018-08-02 23:20:10', NULL),
(5, 'Miscellaneous', 'Ratione quis consequuntur officiis vel enim iste eos. Nam voluptatem maiores omnis veniam consequatur. Dicta ut aliquam sequi fugit ut beatae harum.', 'Y', '2017-09-12 13:40:35', '2018-07-12 02:22:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_video`
--

CREATE TABLE IF NOT EXISTS `gallery_video` (
  `gallery_id` int(10) unsigned NOT NULL,
  `video_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `gallery_video`
--

INSERT INTO `gallery_video` (`gallery_id`, `video_id`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(3, 6),
(3, 7),
(3, 8),
(4, 9),
(4, 10),
(4, 11),
(4, 12),
(5, 13),
(5, 14),
(5, 15),
(5, 16),
(5, 17);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL,
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_10_10_122230_create_professions_table', 1),
(4, '2016_10_10_122544_alter_users_table', 1),
(5, '2016_10_10_123332_create_profiles_table', 1),
(6, '2016_10_10_164406_create_tags_table', 1),
(7, '2016_10_16_161232_create_subjects_table', 1),
(8, '2016_10_18_123526_create_galleries_table', 1),
(9, '2016_10_18_124005_create_videos_table', 1),
(10, '2016_10_18_124205_create_gallery_video_pivot_table', 1),
(11, '2016_10_18_153919_create_articles_table', 1),
(12, '2016_10_19_143206_create_taggables_table', 1),
(13, '2016_10_20_114005_alter_article_table', 1),
(14, '2016_10_20_145752_create_comments_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `professions`
--

CREATE TABLE IF NOT EXISTS `professions` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `professions`
--

INSERT INTO `professions` (`id`, `name`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Student', '2018-08-04 14:40:16', '2018-08-04 14:40:16', NULL),
(2, 'Teacher', '2018-08-04 14:40:16', '2018-08-04 14:40:16', NULL),
(3, 'Programmer', '2018-08-04 14:40:16', '2018-08-04 14:40:16', NULL),
(4, 'Doctor', '2018-08-04 14:40:16', '2018-08-04 14:40:16', NULL),
(5, 'Engineer', '2018-08-04 14:40:16', '2018-08-04 14:40:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `picture` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `web` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `github` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `user_id`, `picture`, `bio`, `web`, `facebook`, `twitter`, `github`, `created_at`, `updated_at`) VALUES
(1, 1, '70adcfcd5adeec9a900d0042da05432e.jpg', 'Quia quos qui repellendus labore rerum aliquid vel. Facere autem atque suscipit tempore sit. Itaque eveniet eaque voluptatem voluptas. Molestiae ex neque ipsum esse reiciendis nisi et enim. Sed consequuntur recusandae eius voluptas qui possimus. Esse error quis ipsam aperiam adipisci voluptatem non. Ut sunt voluptatem rem nulla quasi.', 'http://weber.info/recusandae-voluptatem-autem-asperiores-in-quos-consectetur', 'https://www.facebook.com/Deron', 'https://www.twitter.com/Bria', 'https://www.github.com/Kertzmann', '2018-08-04 14:40:20', '2018-08-04 14:40:20'),
(2, 2, '6660e9f0f262e5b855e1d7b0a14715d1.jpg', 'Temporibus soluta quia non. Voluptatem saepe nisi et sit cupiditate alias unde. Facere rem corrupti voluptatem sed eveniet. Nihil aut neque omnis sapiente ea ex perspiciatis.', 'http://hodkiewicz.com/', 'https://www.facebook.com/Ofelia', 'https://www.twitter.com/Skye', 'https://www.github.com/Lockman', '2018-08-04 14:40:21', '2018-08-04 14:40:21'),
(3, 3, 'c1cd33f29451fec29e757f11fea290b6.jpg', 'Qui est porro molestiae. Provident non animi qui natus. Dolorum natus blanditiis saepe laudantium aut sunt cupiditate est. Voluptatem exercitationem reiciendis facere qui eaque.', 'https://www.bosco.com/minima-dolorem-ut-velit-aliquam-iure-dolores-harum', 'https://www.facebook.com/Josiane', 'https://www.twitter.com/Maude', 'https://www.github.com/Herzog', '2018-08-04 14:40:23', '2018-08-04 14:40:23'),
(4, 4, '7807fe10d9820002ca09fd15af963d5b.jpg', 'Est quod possimus dolorem architecto aut sint quidem eos. Necessitatibus animi aspernatur nulla omnis laudantium natus et. Quos tempore id numquam. Dolorum ex commodi eius dolores animi dolorem sunt.', 'https://www.dietrich.biz/quam-dolor-non-rerum-non-aut-voluptatum', 'https://www.facebook.com/Fatima', 'https://www.twitter.com/Haylee', 'https://www.github.com/Yundt', '2018-08-04 14:40:34', '2018-08-04 14:40:34'),
(5, 5, '0dc4179abd5a933ff8a49a0eaf6009f2.jpg', 'Error nihil laborum aspernatur porro beatae. Ipsa dolorem qui ipsum cumque eos. Quis consectetur consequatur ut voluptatibus est unde. Minus eligendi perferendis dolores modi et.', 'http://bogisich.info/occaecati-consequatur-vero-commodi-repudiandae.html', 'https://www.facebook.com/Ewald', 'https://www.twitter.com/Karelle', 'https://www.github.com/Nader', '2018-08-04 14:40:35', '2018-08-04 14:40:35');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `name`, `description`, `slug`, `created_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Business', 'Culpa sed consequatur molestias sint quo. Quaerat vero aspernatur maiores quis. Eum magni assumenda repellendus reprehenderit culpa. Neque fugit dolorum et. Velit aliquam et nisi enim et dolore est.', 'business', 2, NULL, '2018-05-10 02:01:19', '2018-07-21 11:26:19', NULL),
(2, 'Sports', 'Incidunt ratione laborum quia omnis. Ut qui voluptatem in inventore ut aut eveniet. Qui aut a blanditiis quam voluptas vitae perspiciatis. Illo dolores minima illo quas facere exercitationem facilis.', 'sports', 2, NULL, '2017-11-15 05:50:49', '2018-07-08 00:57:22', NULL),
(3, 'Life Style', 'Est possimus consectetur aut incidunt. Assumenda facere debitis et enim. Maiores aspernatur alias sint et veritatis est quia.', 'life-style', 5, NULL, '2018-07-01 11:14:42', '2018-07-28 12:41:04', NULL),
(4, 'Health', 'Alias perferendis non dolorem et modi. Qui autem voluptatibus veritatis. Eveniet dicta cum totam corporis sunt.', 'health', 4, NULL, '2018-04-14 16:40:16', '2018-07-08 11:58:53', NULL),
(5, 'IT', 'Maxime perferendis in earum soluta quo voluptatem voluptas. Sint aut eum iusto beatae hic aut. Consequuntur ut sapiente culpa aut.', 'it', 1, NULL, '2017-08-25 23:18:18', '2018-07-21 21:15:15', NULL),
(6, 'Politics', 'Ullam fugiat omnis quis similique quia recusandae neque. Et deserunt animi a autem quia. Rerum non quia nisi temporibus odio ut saepe sit.', 'politics', 4, NULL, '2018-03-09 19:32:28', '2018-07-05 10:45:58', NULL),
(7, 'Science', 'Rerum totam rerum quisquam laborum veritatis aut. Quaerat maxime voluptates optio qui nemo. Est eos error ipsam ut dicta delectus magnam. Ipsum sed impedit libero nostrum.', 'science', 3, NULL, '2018-02-26 06:53:58', '2018-07-20 19:49:47', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `taggables`
--

CREATE TABLE IF NOT EXISTS `taggables` (
  `tag_id` int(10) unsigned NOT NULL,
  `taggable_id` int(10) unsigned NOT NULL,
  `taggable_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `taggables`
--

INSERT INTO `taggables` (`tag_id`, `taggable_id`, `taggable_type`) VALUES
(5, 1, 'articles'),
(12, 1, 'articles'),
(6, 1, 'articles'),
(2, 2, 'articles'),
(4, 2, 'articles'),
(10, 2, 'articles'),
(1, 3, 'articles'),
(8, 3, 'articles'),
(2, 4, 'articles'),
(1, 4, 'articles'),
(10, 4, 'articles'),
(4, 5, 'articles'),
(6, 5, 'articles'),
(7, 5, 'articles'),
(5, 5, 'articles'),
(1, 6, 'articles'),
(4, 6, 'articles'),
(7, 7, 'articles'),
(3, 7, 'articles'),
(12, 7, 'articles'),
(5, 7, 'articles'),
(5, 8, 'articles'),
(11, 8, 'articles'),
(2, 8, 'articles'),
(4, 8, 'articles'),
(11, 9, 'articles'),
(4, 9, 'articles'),
(2, 9, 'articles'),
(5, 9, 'articles'),
(11, 10, 'articles'),
(9, 10, 'articles'),
(8, 10, 'articles'),
(2, 11, 'articles'),
(9, 11, 'articles'),
(9, 12, 'articles'),
(7, 12, 'articles'),
(3, 12, 'articles'),
(11, 13, 'articles'),
(10, 13, 'articles'),
(3, 14, 'articles'),
(7, 14, 'articles'),
(12, 15, 'articles'),
(3, 15, 'articles'),
(12, 16, 'articles'),
(2, 16, 'articles'),
(6, 16, 'articles'),
(8, 16, 'articles'),
(2, 17, 'articles'),
(9, 17, 'articles'),
(8, 17, 'articles'),
(4, 18, 'articles'),
(7, 18, 'articles'),
(8, 19, 'articles'),
(12, 19, 'articles'),
(3, 19, 'articles'),
(5, 19, 'articles'),
(3, 20, 'articles'),
(10, 20, 'articles'),
(9, 20, 'articles'),
(6, 20, 'articles'),
(10, 21, 'articles'),
(7, 21, 'articles'),
(3, 21, 'articles'),
(2, 21, 'articles'),
(2, 22, 'articles'),
(11, 22, 'articles'),
(11, 23, 'articles'),
(7, 23, 'articles'),
(6, 23, 'articles'),
(2, 23, 'articles'),
(9, 24, 'articles'),
(12, 24, 'articles'),
(8, 24, 'articles'),
(7, 25, 'articles'),
(4, 25, 'articles'),
(2, 1, 'videos'),
(11, 1, 'videos'),
(6, 1, 'videos'),
(4, 1, 'videos'),
(11, 2, 'videos'),
(1, 2, 'videos'),
(4, 2, 'videos'),
(10, 3, 'videos'),
(9, 3, 'videos'),
(4, 3, 'videos'),
(6, 3, 'videos'),
(11, 4, 'videos'),
(7, 4, 'videos'),
(3, 5, 'videos'),
(2, 5, 'videos'),
(7, 6, 'videos'),
(12, 6, 'videos'),
(3, 6, 'videos'),
(5, 6, 'videos'),
(11, 7, 'videos'),
(4, 7, 'videos'),
(6, 7, 'videos'),
(6, 8, 'videos'),
(9, 8, 'videos'),
(2, 9, 'videos'),
(3, 9, 'videos'),
(7, 9, 'videos'),
(7, 10, 'videos'),
(2, 10, 'videos'),
(11, 11, 'videos'),
(3, 11, 'videos'),
(10, 12, 'videos'),
(6, 12, 'videos'),
(8, 12, 'videos'),
(11, 12, 'videos'),
(3, 13, 'videos'),
(10, 13, 'videos'),
(1, 13, 'videos'),
(5, 13, 'videos'),
(4, 14, 'videos'),
(3, 14, 'videos'),
(7, 14, 'videos'),
(9, 15, 'videos'),
(8, 15, 'videos'),
(1, 15, 'videos'),
(3, 15, 'videos'),
(10, 16, 'videos'),
(9, 16, 'videos'),
(5, 16, 'videos'),
(1, 17, 'videos'),
(6, 17, 'videos'),
(10, 17, 'videos');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `deleted_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `slug`, `created_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Bangladesh', 'bangladesh', 2, NULL, '2017-11-27 05:38:26', '2018-07-12 06:14:05', NULL),
(2, 'Dhaka', 'dhaka', 1, NULL, '2018-03-04 10:46:16', '2018-07-17 13:08:55', NULL),
(3, 'Cricket', 'cricket', 3, NULL, '2017-09-01 15:31:45', '2018-07-24 05:09:32', NULL),
(4, 'Shakib Al Hasan', 'shakib-al-hasan', 1, NULL, '2018-06-20 02:42:22', '2018-07-12 22:45:09', NULL),
(5, 'Mustafizur Rahman', 'mustafizur-rahman', 4, NULL, '2018-03-13 15:01:12', '2018-07-23 01:00:49', NULL),
(6, 'Election', 'election', 3, NULL, '2018-06-28 01:28:13', '2018-08-01 21:42:49', NULL),
(7, 'Champions Trophy 2017', 'champions-trophy-2017', 2, NULL, '2018-01-16 21:08:58', '2018-07-08 11:58:44', NULL),
(8, 'Recipe', 'recipe', 2, NULL, '2018-05-04 22:17:11', '2018-07-27 10:56:41', NULL),
(9, 'US Election', 'us-election', 5, NULL, '2017-08-06 20:12:47', '2018-07-08 01:03:04', NULL),
(10, 'Entertainment', 'entertainment', 5, NULL, '2017-08-08 16:45:47', '2018-07-07 00:39:46', NULL),
(11, 'Nobel Prize', 'nobel-prize', 2, NULL, '2018-06-21 23:09:28', '2018-07-20 02:06:47', NULL),
(12, 'Football', 'football', 1, NULL, '2018-01-20 08:02:19', '2018-08-02 14:32:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `profession_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `profession_id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 5, 'Mrs. Lilly Schaden II', 'zemlak.blaise@example.net', '$2y$10$6J4jQ4Zip2yAYyeKCiK1aO1WMsSUGBtUbwB7gRtPcR0iclxC8ZwX2', 'xX4ZiePjhB', '2018-08-04 14:40:17', '2018-08-04 14:40:17', NULL),
(2, 3, 'Lauren Bins', 'balistreri.freeda@example.com', '$2y$10$ebxlLeGC27DDc9W66NMl3uFO/9jd.O9NuxLjt9rmWv4wESprsJuHy', 'SBJJy0KmHj', '2018-08-04 14:40:18', '2018-08-04 14:40:18', NULL),
(3, 3, 'Rozella Hermann III', 'cremin.dejon@example.com', '$2y$10$P71W2KAEzo5776zasChxj.k/Tu4o8sPG1MTtpk4Xx6NKEz2id9BVO', '0140yGFXmJ', '2018-08-04 14:40:18', '2018-08-04 14:40:18', NULL),
(4, 4, 'Mertie Davis', 'blanda.green@example.com', '$2y$10$71TCDwlGgpsMjo959e3.R.LIw4yH3YepJAsyjZ6xU.BI.iYWgW/92', 'meXfRHB6u5', '2018-08-04 14:40:18', '2018-08-04 14:40:18', NULL),
(5, 5, 'Norberto Gleason', 'austen27@example.org', '$2y$10$qyQ4SP9smRsj5UyRYeIx5e5cU.qHVzWUp4NLhw97Jw2xnJL7nC1.y', 'Ix13YYmmhU', '2018-08-04 14:40:18', '2018-08-04 14:40:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(10) unsigned NOT NULL,
  `provider` enum('Y','F') COLLATE utf8_unicode_ci NOT NULL COMMENT 'Y=Youtube, F=Facebook',
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `summary` text COLLATE utf8_unicode_ci,
  `source` varchar(150) COLLATE utf8_unicode_ci NOT NULL COMMENT 'URL of the file.',
  `display` enum('Y','N') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `videos`
--

INSERT INTO `videos` (`id`, `provider`, `title`, `summary`, `source`, `display`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Y', 'So you want to be a software engineer', 'Quia est voluptas commodi magni ut corrupti ullam libero. Eos voluptatem eos quae quae. Id accusantium et quibusdam veritatis dolorem aliquid atque porro.', 'https://www.youtube.com/embed/WkXzlkOWLE0', 'Y', '2017-10-05 17:12:02', '2018-07-15 23:01:40', NULL),
(2, 'Y', 'How web works', 'Eos mollitia cumque vel debitis et incidunt ad. Ea hic numquam delectus sit officiis explicabo. Reiciendis id minus optio aliquam et nesciunt exercitationem.', 'https://www.youtube.com/embed/uk7xi0q0jms', 'Y', '2017-11-25 07:39:48', '2018-08-03 15:05:18', NULL),
(3, 'Y', 'How can i become a good programmer', 'Iste sequi ut facilis nulla est cupiditate a qui. Incidunt esse est quo provident. Impedit laborum sint qui illo nostrum repellendus.', 'https://www.youtube.com/embed/BjKmWk3oE4E', 'Y', '2018-07-27 00:14:09', '2018-07-05 13:42:35', NULL),
(4, 'Y', 'Job interview for programmer', 'Animi dolores possimus et et rerum ducimus eum. Expedita eaque et possimus. Voluptates qui quos eum eum.', 'https://www.youtube.com/embed/JtKqGa66CTM', 'Y', '2018-06-05 00:33:58', '2018-07-30 00:09:24', NULL),
(5, 'Y', 'Programmer Life', 'Et iusto a sint explicabo est id libero. Commodi magni tempora quia perspiciatis est nihil. Accusamus excepturi velit dolores dolorum officia possimus.', 'https://www.youtube.com/embed/_Nua3Cjdik0', 'Y', '2018-01-23 08:52:39', '2018-07-15 04:16:01', NULL),
(6, 'Y', 'Beautiful Bangladesh - Land Of Stories', 'Voluptatem magni atque praesentium rerum officiis eum. Velit consequatur sit nihil. Minus non vel facere dolore libero voluptas. Beatae blanditiis error aut nisi ipsa.', 'https://www.youtube.com/embed/QNUSIOMb6vI', 'Y', '2017-11-11 09:30:33', '2018-07-14 19:15:29', NULL),
(7, 'Y', 'Bangladesh vat tvc HQ', 'Non iusto et necessitatibus explicabo consequuntur. Minima consequatur corrupti totam eos. Nesciunt reprehenderit autem est ipsa corporis cum placeat quo. Quisquam nostrum veritatis numquam facere.', 'https://www.youtube.com/embed/BpQLRDLePDc', 'Y', '2018-03-27 18:21:04', '2018-07-12 08:23:07', NULL),
(8, 'Y', 'Top 10 Best catches in Bangladesh cricket history', 'Aspernatur qui quas architecto ut. Eaque provident et et vero numquam labore non. Voluptatibus dolores aut quia laudantium nulla. Mollitia et eum et dolores.', 'https://www.youtube.com/embed/hrdqtUGT4Rs', 'Y', '2017-09-06 17:32:55', '2018-07-15 23:31:17', NULL),
(9, 'Y', 'NON STOP ! - MOTIVATIONAL VIDEO', 'Quaerat nam rerum perspiciatis reiciendis. Laborum ipsum nobis quia culpa tempora et sapiente. Eligendi sed quia maiores perferendis non suscipit est. Eaque dolorem non quod sint.', 'https://www.youtube.com/embed/mAwVuRlTYO4', 'Y', '2017-08-24 11:57:09', '2018-07-10 07:09:36', NULL),
(10, 'Y', 'Ripple - Inspirational Tear-jerking Short Film', 'Impedit eius dignissimos sit et. Qui tenetur molestias eveniet rerum. Sed nisi placeat rerum est. Id ut iste excepturi.', 'https://www.youtube.com/embed/ovj5dzMxzmc', 'Y', '2018-06-08 19:31:28', '2018-07-27 10:21:57', NULL),
(11, 'Y', 'My Dad is a Liar', 'Autem velit quia inventore consequatur aut est ut. Id eum ut aperiam qui. Et autem cum laboriosam corporis mollitia et.', 'https://www.youtube.com/embed/EZgmj5ay5Bk', 'Y', '2018-07-19 17:31:43', '2018-07-17 08:40:20', NULL),
(12, 'Y', 'Stay hungry...Stay foolish', 'Fugiat omnis voluptates sunt. Quidem omnis sint et itaque atque. Eius et necessitatibus rem quasi nulla natus. Ipsa eius aut omnis vel sit ut. Et quaerat a earum rerum repellendus.', 'https://www.youtube.com/embed/gO6cFMRqXqU', 'Y', '2017-12-21 01:37:40', '2018-07-11 06:07:01', NULL),
(13, 'Y', 'These Powerful Pictures speaks more than anything', 'Tenetur quas vitae exercitationem. Eum rerum reiciendis natus molestiae. Temporibus voluptatibus voluptatem labore quibusdam dolorum rerum.', 'https://www.youtube.com/embed/6ZTesf5pNjw', 'Y', '2018-06-20 05:04:41', '2018-07-31 17:28:47', NULL),
(14, 'Y', 'Together Everyone Achieves More', 'Eos optio aspernatur maiores incidunt. Nulla optio in natus ut. Eum cupiditate nesciunt mollitia necessitatibus illum ipsam et. Esse sint voluptatem repellendus repudiandae.', 'https://www.youtube.com/embed/OVf3T3pgL8U', 'Y', '2018-02-08 09:24:45', '2018-07-18 00:18:34', NULL),
(15, 'Y', 'Good teamwork and bad teamwork', 'Rem vero quis odit natus fugiat est aut. Fugiat fuga laborum eveniet molestiae. Sequi ut recusandae ab. Et ducimus quo labore provident quo. Quam voluptatem et voluptatem mollitia.', 'https://www.youtube.com/embed/fUXdrl9ch_Q', 'Y', '2018-04-21 17:23:36', '2018-07-24 10:50:03', NULL),
(16, 'Y', 'This is your life', 'Cum quia reprehenderit ab. Quibusdam eos asperiores et vel ut pariatur voluptas et. Voluptas dolores ut omnis id. Nesciunt optio eveniet debitis et vitae sit. Sunt molestiae quia fugit.', 'https://www.youtube.com/embed/RL-lfcihHvU', 'Y', '2018-06-17 05:58:23', '2018-07-22 18:09:57', NULL),
(17, 'F', 'Motivational Videos for Students', 'Odit enim explicabo voluptas et consequuntur. Fugit et et totam dolores optio minima dolor. Assumenda ut porro repellendus laudantium. Veritatis vitae a ipsum totam.', 'https://www.facebook.com/wubedubd/videos/10154365905494972/', 'Y', '2017-10-03 08:14:04', '2018-07-21 03:55:08', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `articles_user_id_foreign` (`user_id`),
  ADD KEY `articles_subject_id_foreign` (`subject_id`),
  ADD KEY `articles_slug_index` (`slug`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_user_id_foreign` (`user_id`),
  ADD KEY `comments_commentable_id_index` (`commentable_id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_video`
--
ALTER TABLE `gallery_video`
  ADD PRIMARY KEY (`gallery_id`,`video_id`),
  ADD KEY `gallery_video_video_id_foreign` (`video_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `professions`
--
ALTER TABLE `professions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `professions_name_unique` (`name`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `profiles_user_id_foreign` (`user_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `subjects_name_unique` (`name`),
  ADD KEY `subjects_created_by_foreign` (`created_by`),
  ADD KEY `subjects_deleted_by_foreign` (`deleted_by`),
  ADD KEY `subjects_slug_index` (`slug`);

--
-- Indexes for table `taggables`
--
ALTER TABLE `taggables`
  ADD KEY `taggables_tag_id_foreign` (`tag_id`),
  ADD KEY `taggables_taggable_id_index` (`taggable_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_name_unique` (`name`),
  ADD KEY `tags_created_by_foreign` (`created_by`),
  ADD KEY `tags_deleted_by_foreign` (`deleted_by`),
  ADD KEY `tags_slug_index` (`slug`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_profession_id_foreign` (`profession_id`);

--
-- Indexes for table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `videos_source_unique` (`source`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=236;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `professions`
--
ALTER TABLE `professions`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `videos`
--
ALTER TABLE `videos`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `articles_subject_id_foreign` FOREIGN KEY (`subject_id`) REFERENCES `subjects` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `articles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `gallery_video`
--
ALTER TABLE `gallery_video`
  ADD CONSTRAINT `gallery_video_gallery_id_foreign` FOREIGN KEY (`gallery_id`) REFERENCES `galleries` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `gallery_video_video_id_foreign` FOREIGN KEY (`video_id`) REFERENCES `videos` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subjects`
--
ALTER TABLE `subjects`
  ADD CONSTRAINT `subjects_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `subjects_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `taggables`
--
ALTER TABLE `taggables`
  ADD CONSTRAINT `taggables_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE SET NULL,
  ADD CONSTRAINT `tags_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_profession_id_foreign` FOREIGN KEY (`profession_id`) REFERENCES `professions` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
